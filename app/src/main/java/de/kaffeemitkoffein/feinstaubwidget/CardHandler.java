/**

        This file is part of FeinstaubWidget.

        Copyright (c) 2018, 2019 Pawel Dube

        FeinstaubWidget is free software: you can redistribute it and/or modify it
        under the terms of the GNU General Public License as published by the
        Free Software Foundation, either version 3 of the License, or (at
        your option) any later version.

        FeinstaubWidget is distributed in the hope that it will be useful, but
        WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
        General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

*/

package de.kaffeemitkoffein.feinstaubwidget;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import android.net.Uri;
import android.os.AsyncTask;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * This class handles the data transfer from the Luftdaten-API (json) to the local
 * database (SQLite).
 *
 * This class has no public constructor. Obtain an instance by
 *
 * CardHandler ch = CardHandler.getInstance();
 */

public class CardHandler {

    public static final Uri DATABASE_URI = Uri.parse("content://de.kaffeemitkoffein.feinstaubwidget/folder");
    public static final String LUFTDATEN_API_URL="https://api.luftdaten.info/v1/sensor/";

    public static final String[] SQL_COMMAND_QUERYCOLUMNS = {
            LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_id,
            LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_sensordataValue0,
            LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_sensordataValue1,
            LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_sensorId,
            LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_sensorTypeName,
            LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_timestamp,
            LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_locationAltitude,
            LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_locationLongitude,
            LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_locationLatitude,
            LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_locationCountry,
            LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_timestampUTCmillis};

    public static final String SQL_COMMAND_QUERYALL = "SELECT * FROM " + LuftDatenContentProvider.LuftDatenDatabaseHelper.TABLE_NAME;

    private Context context;
    private ContentResolver contentResolver;

    public CardHandler(Context c){
        this.context = c;
        contentResolver = context.getApplicationContext().getContentResolver();
    }

    /**
     * Adds a sensorDataSet to the database.
     * To avoid duplicates, please consider using addSensorDataSetIfNew(sensorDataSet).
     *
     * @param sensorDataSet
     */

    public void addSensorDataSet(SensorDataSet sensorDataSet){
        contentResolver.insert(LuftDatenContentProvider.URI_SENSORDATA,getContentValuesFromSensorDataSet(sensorDataSet));
    }

    /**
     * Builds a ContentValues instance from a SensorDataSet.
     *
     * @param sensorDataSet
     * @return ContentValues
     */

    public ContentValues getContentValuesFromSensorDataSet(SensorDataSet sensorDataSet){
        ContentValues card = new ContentValues();
        card.put(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_sensordataValue0,sensorDataSet.sensordataValue[0]);
        card.put(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_sensordataValue1,sensorDataSet.sensordataValue[1]);
        card.put(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_sensorId,sensorDataSet.sensorId);
        card.put(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_sensorTypeName,sensorDataSet.sensorTypeName);
        card.put(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_timestamp,sensorDataSet.timestamp);
        card.put(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_locationAltitude,sensorDataSet.locationAltitude);
        card.put(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_locationLongitude,sensorDataSet.locationLongitude);
        card.put(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_locationLatitude,sensorDataSet.locationLatitude);
        card.put(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_locationCountry,sensorDataSet.locationCountry);
        card.put(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_timestampUTCmillis,sensorDataSet.getTimestampUTCMillis());
        return card;
    }


    /**
     * Adds a SensorDataSet to the database if the data is new.
     * Matches sensor number and timestamp of the data set.
     *
     * @param sensorDataSet
     * A data set candidate to add to the data base. Must not be null.
     *
     * @return
     * true if the data set was added
     * false if the data set is already present in the database
     */

    public Boolean addSensorDataSetIfNew(SensorDataSet sensorDataSet) {
        SensorDataSet card = getLatestDataSet(sensorDataSet.sensorId);
        if (card != null) {
                if (sensorDataSet.getTimestampUTCMillis() > card.getTimestampUTCMillis()){
                    addSensorDataSet(sensorDataSet);
                    return true;
                }
        } else {
            // add data, because database is empty for this sensor.
            addSensorDataSet(sensorDataSet);
            return true;
        }
        return false;
    }

    public Boolean addSensorDataSetIfNew(FullSensorDataSet fullsensorDataSet) {
        return addSensorDataSetIfNew(fullsensorDataSet.toSensorDataSet());
    }



    /**
     * Reads a SensorDataSet from the cursor c.
     * @param c Cursor
     * @return
     */
    private SensorDataSet getSensorDataSetFromCursor(Cursor c){
        SensorDataSet card = new SensorDataSet();
        card.sensordataValue[0] = c.getString(c.getColumnIndex(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_sensordataValue0));
        card.sensordataValue[1] = c.getString(c.getColumnIndex(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_sensordataValue1));
        card.sensorId = c.getString(c.getColumnIndex(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_sensorId));
        card.sensorTypeName = c.getString(c.getColumnIndex(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_sensorTypeName));
        card.timestamp = c.getString(c.getColumnIndex(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_timestamp));
        card.locationAltitude = c.getString(c.getColumnIndex(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_locationAltitude));
        card.locationLongitude = c.getString(c.getColumnIndex(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_locationLongitude));
        card.locationLatitude = c.getString(c.getColumnIndex(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_locationLatitude));
        card.locationCountry = c.getString(c.getColumnIndex(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_locationCountry));
        card.timestamp_UTCmillis = c.getLong(c.getColumnIndex(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_timestampUTCmillis));
        card.database_id = c.getInt(c.getColumnIndex(LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_id));
        return card;
    }

    /**
     * Gets a single sensorDataSet from the database.
     * @param i number of the data set.
     * @return
     */

    public SensorDataSet getSensorDataSet(int i){
        SensorDataSet card = new SensorDataSet();
        Cursor c = null;
        try {
            c = contentResolver
                       .query(LuftDatenContentProvider.URI_SENSORDATA,
                               SQL_COMMAND_QUERYCOLUMNS,
                               LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_id+" = ?",
                               new String[] {String.valueOf(i)},null);
            if (c != null) {
                if (c.moveToFirst()){
                    card = getSensorDataSetFromCursor(c);
                }
            }
        } finally {
            if (c!=null){
                c.close();
            }
        }
        return card;
    }

    /**
     * Gets the sensor data from the SQLite database for the specified sensor.
     *
     * @param sensor1
     * @return ArrayList of SensorDataSet
     *
     * The ArrayList ist sorted by timestamp (UTC in millis) in descending order,
     * meaning that position 1 of the ArrayList will hold the latest data set.
     */
    public ArrayList<SensorDataSet> getSensorDataSetArray(String sensor1){
        ArrayList<SensorDataSet> cards = new ArrayList<SensorDataSet>();
        Cursor c = null;
        try {
            String selection = LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_sensorId + " = ?";
            String[] selectionArguments = {sensor1};
            String order = LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_timestampUTCmillis + " DESC";
            c = contentResolver
                    .query(LuftDatenContentProvider.URI_SENSORDATA,
                            SQL_COMMAND_QUERYCOLUMNS,
                            selection,
                            selectionArguments,order);
            if (c.moveToFirst()) {
                do {
                    SensorDataSet card = getSensorDataSetFromCursor(c);
                    cards.add(card);
                } while (c.moveToNext());
                }
        } catch (Exception e){
            // nothing to do
        } finally {
            if (c!=null)
            c.close();
        }
        return cards;
    }

    /**
     * Gets the sensor data from the SQLite database for the specified sensor,
     * returning data within the timespan in milliseconds from now.
     *
     * @param sensor1
     * @param millirange
     * @return ArrayList of SensorDataSet
     *
     * The ArrayList ist sorted by timestamp (UTC in millis) in descending order,
     * meaning that position 1 of the ArrayList will hold the latest data set.
     *
     */

    public ArrayList<SensorDataSet> getSensorDataSetArrayByTimeInterval(String sensor1, Long millirange){
        ArrayList<SensorDataSet> cards = new ArrayList<SensorDataSet>();
        Cursor c = null;
        Calendar calendar = Calendar.getInstance();
        long l = calendar.getTimeInMillis() - millirange;
        String selection = LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_sensorId + " = ? AND " + LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_timestampUTCmillis + " > ?";
        String[] selectionArguments = {sensor1, String.valueOf(l)};
        String order = LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_timestampUTCmillis + " DESC";
        try {
            c = contentResolver
                    .query(LuftDatenContentProvider.URI_SENSORDATA,SQL_COMMAND_QUERYCOLUMNS,selection,selectionArguments,order);

            // c = sql_db.query(TABLE_NAME,SQL_COMMAND_QUERYCOLUMNS,selection,selectionArguments,null,null,order,null);
            if (c.moveToFirst()) {
                do {
                    SensorDataSet card = getSensorDataSetFromCursor(c);
                    cards.add(card);
                } while (c.moveToNext());
            }
        } catch (Exception SQLiteException) {
            // onCreate(sql_db);
        }
        if (c!=null)
            c.close();
        return cards;
    }

    /**
     *
     * Get all available sensorData from the data base.
     *
     * @return ArrayList of SensorDataSet
     *
     * The arraylist ist not sorted.
     */

    public ArrayList<SensorDataSet> getSensorDataSetArray() {
        ArrayList<SensorDataSet> cards = new ArrayList<SensorDataSet>();
        Cursor c = null;
        try {
            c = contentResolver.query(LuftDatenContentProvider.URI_SENSORDATA,SQL_COMMAND_QUERYCOLUMNS,null,null,null);
            // c = sql_db.rawQuery(SQL_COMMAND_QUERYALL, null);
            if (c.moveToFirst()) {
                do {
                    SensorDataSet card = getSensorDataSetFromCursor(c);
                    cards.add(card);
                } while (c.moveToNext());
            }
        } catch (Exception SQLiteException){
            // onCreate(sql_db);
        }
        if (c!=null)
            c.close();
        return cards;
    }

    /**
     * Deletes a single sensorDataSet.
     *
     * @param i
     */

    public int deleteSensorDataSet(int i){
        int rows = 0;
        try {
            rows = contentResolver.delete(LuftDatenContentProvider.URI_SENSORDATA,LuftDatenContentProvider.LuftDatenDatabaseHelper.KEY_id+"=?",new String[] {String.valueOf(i)});
            //rows = sql_db.delete(TABLE_NAME, KEY_id+"=?", new String[] {String.valueOf(i)});
        } catch (Exception e) {
            // do nothing here
        }
        return rows;
    }

    private class Async_DatabaseCleaner extends AsyncTask<Void, Void, Integer>{
        private ArrayList<SensorDataSet> cards;
        private Date before;

        public Async_DatabaseCleaner(ArrayList<SensorDataSet> data, Date date){
            cards = data;
            before = date;
        }

        @Override
        protected Integer doInBackground(Void... voids){
            int size = cards.size();
            int deleted_count = 0;
            Calendar c = Calendar.getInstance();
            c.setTime(before);
            for (int i=0; i<size; i++){
                SensorDataSet sensorDataSet = cards.get(i);
                if (sensorDataSet.getTimestampLocalMillis() < c.getTimeInMillis()) {
                    deleteSensorDataSet(sensorDataSet.database_id);
                    deleted_count = deleted_count + 1;
                }
            }
            return deleted_count;
        }

        @Override
        protected void onPostExecute(Integer i){
          // do nothing at the moment
        }
    }

    public void sync_DatabaseCleaner(ArrayList<SensorDataSet> data, Date before){
        int size = data.size();
        int deleted_count = 0;
        Calendar c = Calendar.getInstance();
        c.setTime(before);
        for (int i=0; i<size; i++) {
            SensorDataSet sensorDataSet = data.get(i);
            if (sensorDataSet.getTimestampLocalMillis() < c.getTimeInMillis()) {
                deleteSensorDataSet(sensorDataSet.database_id);
                deleted_count = deleted_count + 1;
            }
        }
    }

    /**
     * Deletes entires before a specified date.
     *
     * asyncdelete specifies if the clean-up has to be done in the background (#true) or on the
     * main thread (#false). For the widgets, it is safer to do this on the main thread, while
     * within an activity this should be done in the background.
     *
     * @param before
     * @param asyncdelete
     */

    public void deleteOldEntries(Date before, Boolean asyncdelete){
        ArrayList<SensorDataSet> cards = getSensorDataSetArray();
        if (asyncdelete){
            Async_DatabaseCleaner asc = new Async_DatabaseCleaner(cards,before);
            asc.execute();
        } else {
            sync_DatabaseCleaner(cards,before);
        }
    }


    /**
     * Returns the newest dataset for the specified sensor.
     *
     * @param sensor
     * @return s sensorDataSet
     */

    public SensorDataSet getLatestDataSet(String sensor){
        ArrayList<SensorDataSet> cards = getSensorDataSetArray(sensor);
        if (cards.size() > 0) {
            return cards.get(0);
        } else {
            return null;
        }
    }

    /**
     * Returns the Lufdaten.info API url for the specified sensor.
     *
     * @param sensor
     * @return url The sensor url.
     */

    public URL getSensorAPIURL(String sensor){
        try {
            URL url = new URL(LUFTDATEN_API_URL+sensor+"/");
            return url;
        } catch (Exception e){
            return null;
        }
    }



}
