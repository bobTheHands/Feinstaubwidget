/**

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class StaubDaten {

    private ArrayList<SensorDataSet> sensorDataSets = new ArrayList<>();
    private int datasets = 0;

    public int length(){
        return datasets;
    }

    public SensorDataSet getDataSet(int i){
        return sensorDataSets.get(i);
    }

    public void addDataSet(SensorDataSet sensorDataSet){
        sensorDataSets.add(sensorDataSet);
    }

    public void resetDataSets(){
        datasets = 0;
        sensorDataSets.clear();
    }

    public ArrayList<SensorDataSet> getDataSetArray(){
        return sensorDataSets;
    }

    public String getSensorTypeName(int position){
        SensorDataSet sensorDataSet = getDataSet(position);
        return sensorDataSet.sensorTypeName;
    }

    public ArrayList<SensorDataSet> readDataFromJSONString(String datastring){
    JSONArray sensorData = getSensorDataArray(datastring);
    if (sensorData != null) {
        for (int i=0; i<=sensorData.length() ;i++){
            try {
                sensorDataSets.add(getDataFromJSON(sensorData.getJSONObject(i)));
                datasets = datasets + 1;
            } catch (Exception e){
                // nothing to do
            }
        }
    }
    return sensorDataSets;
    }

    private JSONArray getSensorDataArray(String datastring){
        try {
            JSONArray ja = new JSONArray(datastring);
            return ja;
        } catch (Exception e) {
            return null;
        }
    }

    private SensorDataSet getDataFromJSON(JSONObject jobject){
        SensorDataSet sensorDataSet = new SensorDataSet();
        String valueArray = getValueArray(jobject);
        if (valueArray != null){
            sensorDataSet = getValues(sensorDataSet,valueArray);
        }
        String sensorArray = getSensorArray(jobject);
        if (sensorArray != null){
            sensorDataSet = getSensor(sensorDataSet,sensorArray);
        }
        try {
            sensorDataSet.timestamp = jobject.getString("timestamp");
        } catch (Exception e){
            // nothing to do
        }
        String locationArray = getLocationArray(jobject);
        if (locationArray != null)
            sensorDataSet = getLocation(sensorDataSet,locationArray);
        return sensorDataSet;
    }

    private String getValueArray(JSONObject jobject){
        try {
            String s = jobject.getString("sensordatavalues");
            return s;
        } catch (Exception e){
            return null;
        }
    }

    private SensorDataSet getValues(SensorDataSet sensorDataSet, String s){
        try {
            JSONArray dataarray = new JSONArray(s);
            JSONObject data0 = dataarray.getJSONObject(0);
            JSONObject data1 = dataarray.getJSONObject(1);
            String type0 = data0.getString("value_type");
            String type1 = data1.getString("value_type");
            // fix to adapt to flexible database order, whatever of both comes first: temperature or humidity, P1 or P2
            // the order in the local data is:
            // 0 => temperature or P1
            // 1 => humidity or P2
            if (type0.equals("humidity")){
                sensorDataSet.sensordataValue[0] = data1.getString("value");
                sensorDataSet.sensordataValue[1] = data0.getString("value");
            } else
            {
                sensorDataSet.sensordataValue[0] = data0.getString("value");
                sensorDataSet.sensordataValue[1] = data1.getString("value");
            }
            if (type0.equals("P2")){
                sensorDataSet.sensordataValue[0] = data1.getString("value");
                sensorDataSet.sensordataValue[1] = data0.getString("value");
            }
            return sensorDataSet;
        } catch (Exception e){
          // nothing to do
            return sensorDataSet;
        }
    }

    private String getSensorArray(JSONObject jobject){
        try {
            String s = jobject.getString("sensor");
            return s;
        } catch (Exception e){
            return null;
        }
    }

    private SensorDataSet getSensor(SensorDataSet sensorDataSet, String s){
        try{
            JSONObject sensor = new JSONObject(s);
            sensorDataSet.sensorId = sensor.getString("id");
            String sensortype = sensor.getString("sensor_type");
            JSONObject sensorType = new JSONObject(sensortype);
            sensorDataSet.sensorTypeName = sensorType.getString("name");
            return sensorDataSet;
        } catch (Exception e){
            return  sensorDataSet;
        }
    }

    private String getLocationArray(JSONObject jobject){
        try {
            String s = jobject.getString("location");
            return s;
        } catch (Exception e){
            return null;
        }
    }

    private SensorDataSet getLocation(SensorDataSet sensorDataSet, String s){
        try {
            JSONObject location = new JSONObject(s);
            sensorDataSet.locationAltitude = location.getString("altitude");
            sensorDataSet.locationLongitude = location.getString("longitude");
            sensorDataSet.locationLatitude = location.getString("latitude");
            sensorDataSet.locationCountry = location.getString("country");
            return sensorDataSet;
        } catch (Exception e) {
            return sensorDataSet;
        }
    }

}
