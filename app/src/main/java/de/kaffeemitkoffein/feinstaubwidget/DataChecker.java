/**

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import android.content.Context;
import android.graphics.Color;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * This class checks the connectivity for a sensor number.
 *
 * It extends the FetchSensorDataFromAPI class to perform a connectivity check.
 *
 * The results are directly displayed by updating the views given to the public constructor.
 * See below for more details.
 *
 */

public class DataChecker extends FetchSensorDataFromAPI {

    private ImageView resultGraphics;
    private TextView resultText;
    private int expected_sensortype;
    private Boolean do_sensorguess = false;

    /**
     * Public constructor for this class.
     *
     * The result is presented by updating the given views:
     *
     * @param c     activity context.
     * @param iv    ImageView where to place the conn.-result icon. May be null.
     * @param tv    TextView where to place the conn.-result text. May be null.
     * @param est   Expected sensor type. May be null to ignore check for expected sensor type.
     * @param b     Sets if an auto-guess of the temperature sensor should be performed. This makes the class to call the autoSearchNextSensor() sub, which then needs be overridden with the desired task.
     *
     *
     */

    public DataChecker(Context c, ImageView iv, TextView tv, int est, Boolean b){
        super(c);
        context = c;
        resultGraphics = iv;
        resultText = tv;
        expected_sensortype = est;
        do_sensorguess = b;
        resultGraphics.setImageResource(R.mipmap.ic_sync_white_24dp);
        resultText.setText(context.getResources().getString(R.string.login_connresult_trying));
        resultText.setTextColor(Color.YELLOW);
        }


    /**
     * Override this sub with the routines/call to auto-guess the temperature sensor.
     * It does nothing by default.
     *
     * This sub is called finally after a connection was successfully made. It is not called if the conn. fails.
     *
     */

    public void autoSearchNextSensor(){
        // add action to search for sensor here.
    }

    @Override
    public void onPositiveResult(ArrayList<Integer> sensortypes){
        // updateWidgetDisplay(context,AppWidgetManager.getInstance(context),AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context,StaubWidget.class)));
        int sensortype = SensorDataSet.UNKNOWN_SENSOR;
        if (sensortypes.size()>0)
                sensortype = sensortypes.get(0);
        if (sensortypes.size()==0){
            if (resultGraphics != null) {
                resultGraphics.setImageResource(R.mipmap.ic_warning_white_24dp);
            }
            if (resultText != null) {
                resultText.setText(context.getResources().getString(R.string.login_connresult_nodata));
                resultText.setTextColor(Color.YELLOW);
            }
        } else {
            if (sensortype == expected_sensortype){
                if (resultGraphics != null) {
                    resultGraphics.setImageResource(R.mipmap.ic_https_white_24dp);
                }
                if (resultText != null) {
                    resultText.setText(context.getResources().getString(R.string.login_connresult_ok));
                    resultText.setTextColor(Color.GREEN);
                }
            } else {
                if (resultGraphics != null) {
                    resultGraphics.setImageResource(R.mipmap.ic_error_white_24dp);
                }
                if (resultText != null) {
                    resultText.setText(context.getResources().getString(R.string.login_wrongsensortype));
                    resultText.setTextColor(Color.YELLOW);
                    }
                }
                if (do_sensorguess){
                    autoSearchNextSensor();
                }
            }
        }

    @Override
    public void onNegativeResult(){
        if (resultGraphics != null) {
            resultGraphics.setImageResource(R.mipmap.ic_sync_problem_white_24dp);
        }
        if (resultText != null) {
            resultText.setText(context.getResources().getString(R.string.login_connresult_fail));
            resultText.setTextColor(Color.RED);
        }
    }
}
