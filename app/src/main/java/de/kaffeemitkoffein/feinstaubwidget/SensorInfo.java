/**

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;

import java.net.URL;
import java.util.ArrayList;

import android.widget.Toast;

public class SensorInfo extends Activity {

    private String sensorid="";
    private FullSensorDataSet sensor;
    private FeinstaubPreferences preferences;
    private Boolean nodata = false;

    private TextView textview_sensorid;
    private TextView textview_timestamp;
    private TextView textview_typename;
    private TextView textview_typeid;
    private TextView textview_manufacturer;
    private TextView textview_samplingrate;
    private TextView textview_pin;
    private TextView textview_country;
    private TextView textview_locationid;
    private TextView textview_latitude;
    private TextView textview_longitude;
    private TextView textview_altitude;
    private TableLayout tablelayout_currentdata;
    private TextView textview_valueid0;
    private TextView textview_valuetype0;
    private TextView textview_value0;
    private TextView textview_valueid1;
    private TextView textview_valuetype1;
    private TextView textview_value1;


    private final static String PARCEL_DATA   = "PARCEL_DATA";
    private final static String PARCEL_NODATA = "PARCEL_NODATA";

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putParcelable(PARCEL_DATA,sensor);
        savedInstanceState.putBoolean(PARCEL_NODATA,nodata);
    }

    @Override
    public void onRestoreInstanceState(Bundle restoredInstanceState) {
        super.onRestoreInstanceState(restoredInstanceState);
        nodata = restoredInstanceState.getBoolean(PARCEL_NODATA,false);
        sensor = restoredInstanceState.getParcelable(PARCEL_DATA);
        displayData();
    }

    @Override
    protected void onResume(){
        super.onResume();
        readPreferences();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensorinfo);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        readPreferences();
        Button button_add1 = (Button) findViewById(R.id.sensorinfo_button_add1);
        button_add1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerAsLocalSensor(1);
            }
        });
        Button button_add2 = (Button) findViewById(R.id.sensorinfo_button_add2);
        button_add2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerAsLocalSensor(2);
            }
        });
        Button button_back = (Button) findViewById(R.id.sensorinfo_button_back);
        button_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        if (savedInstanceState!=null){
            sensor = savedInstanceState.getParcelable(PARCEL_DATA);
        } else {
            Intent i = getIntent();
            Bundle bundle = i.getExtras();
            if (bundle != null) { ;
                sensorid = bundle.getString(MainActivity.DATA_SENSORNUMBER, "");
            }
            View mainView = getWindow().getDecorView().getRootView();
            mainView.post(new Runnable() {
                @Override
                public void run() {
                    fetchSensorData(sensorid);
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem mi) {
        int item_id = mi.getItemId();
        if (item_id == android.R.id.home) {
            /*
             * Unify the action of the actionbar home button to do the same
             * as the back button.
             */
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(mi);
    }

    private void displayNoDataInfo(){
        Runnable r = new Runnable() {
            @Override
            public void run() {
                TextView textView = (TextView) findViewById(R.id.sensorinfo_nodata);
                if (textView!=null){
                    textView.setText(R.string.sensorinfo_nodata_info);
                    textView.setVisibility(View.VISIBLE);
                    textView.invalidate();
                }
            }
        };
        this.runOnUiThread(r);
    }

    private void fetchSensorData(final String sensorid){
        final CardHandler ch = new CardHandler(this);
        URL sensor_url = ch.getSensorAPIURL(sensorid);
        FetchSensorDataFromAPI reader = new FetchSensorDataFromAPI(this){
            @Override
            protected void onPostExecute(ArrayList<FullSensorDataSet> content) {
                if (content != null){
                    if (content.size()>0){
                        // reading data was successful
                        sensor = content.get(0);
                        ch.addSensorDataSetIfNew(sensor);
                        nodata = false;
                        displayData();
                    } else {
                        // reading data failed
                        SensorDataSet sensorDataSet = ch.getLatestDataSet(sensorid);
                        try {
                            sensor = sensorDataSet.toFullSensorDataSet();
                        } catch (NullPointerException e){
                            // nothing to do, as we simply have no data to display.
                        }
                        nodata = true;
                        displayData();
                        displayNoDataInfo();
                        // Toast.makeText(context,getApplication().getResources().getString(R.string.sensorinfo_nodata_info),Toast.LENGTH_LONG).show();
                    }
                }
            }
        };
        reader.execute(sensor_url);
    }

    private void registerAsLocalSensor(int sensornumber){
        if (sensor == null)
            return;
        if (sensor.sensorId == null)
            return;
        if (sensor.sensorId.equals(""))
            return;
        if (sensornumber==1){
            preferences.sensor1_number = sensor.sensorId;
            preferences.commitPreference(FeinstaubPreferences.PREF_SENSOR1_NUMBER,preferences.sensor1_number);
            Toast.makeText(this,getApplication().getResources().getString(R.string.sensorinfo_added_info),Toast.LENGTH_SHORT).show();
            displayData();
        } else if (sensornumber == 2){
            preferences.sensor2_number = sensor.sensorId;
            preferences.commitPreference(FeinstaubPreferences.PREF_SENSOR2_NUMBER,preferences.sensor2_number);
            Toast.makeText(this,getApplication().getResources().getString(R.string.sensorinfo_added_info),Toast.LENGTH_SHORT).show();
            displayData();
        }
    }

    private void displayData(){
        if (nodata)
            displayNoDataInfo();
        if (sensor == null)
            return;
        textview_sensorid       = (TextView) findViewById(R.id.sensorinfo_sensorid_value);
        textview_timestamp      = (TextView) findViewById(R.id.sensorinfo_timestamp_value);
        textview_typename       = (TextView) findViewById(R.id.sensorinfo_typename_value);
        textview_typeid         = (TextView) findViewById(R.id.sensorinfo_typeid_value);
        textview_manufacturer   = (TextView) findViewById(R.id.sensorinfo_manufacturer_value);
        textview_samplingrate   = (TextView) findViewById(R.id.sensorinfo_samplingrate_value);
        textview_pin            = (TextView) findViewById(R.id.sensorinfo_pin_value);
        textview_country        = (TextView) findViewById(R.id.sensorinfo_country_value);
        textview_locationid     = (TextView) findViewById(R.id.sensorinfo_locationid_value);
        textview_latitude       = (TextView) findViewById(R.id.sensorinfo_latitude_value);
        textview_longitude      = (TextView) findViewById(R.id.sensorinfo_longitude_value);
        textview_altitude       = (TextView) findViewById(R.id.sensorinfo_altitude_value);
        tablelayout_currentdata = (TableLayout) findViewById(R.id.sensorinfo_table_currentdata);
        textview_valueid0       = (TextView) findViewById(R.id.sensorinfo_valueid0);
        textview_valuetype0     = (TextView) findViewById(R.id.sensorinfo_valuetype0);
        textview_value0         = (TextView) findViewById(R.id.sensorinfo_value0);
        textview_valueid1       = (TextView) findViewById(R.id.sensorinfo_valueid1);
        textview_valuetype1     = (TextView) findViewById(R.id.sensorinfo_valuetype1);
        textview_value1         = (TextView) findViewById(R.id.sensorinfo_value1);
        if ((textview_sensorid != null) && (sensor.sensorId != null)){
            String text = sensor.sensorId;
            if (preferences.sensor1_number.equals(sensor.sensorId)){
                text = text + " ("+getApplication().getResources().getString(R.string.sensorinfo_sensorid_known)+"1)";
            } else if (preferences.sensor2_number.equals(sensor.sensorId)) {
                text = text + " (" + getApplication().getResources().getString(R.string.sensorinfo_sensorid_known) + "2)";
            }
            textview_sensorid.setText(text);
        }
        if ((textview_timestamp!=null) && (sensor.timestamp != null)){
            textview_timestamp.setText(sensor.timestamp);
        }
        if ((textview_typename!=null) && (sensor.sensorTypeName != null)){
            textview_typename.setText(sensor.sensorTypeName);
        }
        if ((textview_typeid!=null) && (sensor.sensorTypeId != null)){
            textview_typeid.setText(sensor.sensorTypeId);
        }
        if ((textview_manufacturer!=null) && (sensor.manufacturer != null)){
            textview_manufacturer.setText(sensor.manufacturer);
        }
        if (textview_samplingrate!=null) {
            if (sensor.samplingRate != null){
                textview_samplingrate.setText(sensor.samplingRate);
            } else {
                textview_samplingrate.setText("-");
            }
        }
        if ((textview_pin!=null) && (sensor.sensorPin != null)){
            textview_pin.setText(sensor.sensorPin);
        }
        if ((textview_country!=null) && (sensor.locationCountry != null)){
            textview_country.setText(sensor.locationCountry);
        }
        if ((textview_locationid!=null) && (sensor.locationId != null)){
            textview_locationid.setText(sensor.locationId);
        }
        if ((textview_latitude!=null) && (sensor.locationLatitude != null)){
            textview_latitude.setText(sensor.locationLatitude);
        }
        if ((textview_longitude!=null) && (sensor.locationLongitude != null)){
            textview_longitude.setText(sensor.locationLongitude);
        }
        if ((textview_altitude!=null) && (sensor.locationAltitude != null)){
            textview_altitude.setText(sensor.locationAltitude);
        }
        if (tablelayout_currentdata !=null){
            if ((textview_valueid0 != null) && (sensor.sensordataValueId[0] != null)){
                textview_valueid0.setText(sensor.sensordataValueId[0]);
            }
            if ((textview_valuetype0 != null) && (sensor.sensordataValueType[0] != null)){
                textview_valuetype0.setText(sensor.sensordataValueType[0]);
            }
            if ((textview_value0 != null) && (sensor.sensordataValue[0] != null)){
                textview_value0.setText(sensor.sensordataValue[0]);
            }
            if ((textview_valueid1 != null) && (sensor.sensordataValueId[1] != null)){
                textview_valueid1.setText(sensor.sensordataValueId[1]);
            }
            if ((textview_valuetype1 != null) && (sensor.sensordataValueType[1] != null)){
                textview_valuetype1.setText(sensor.sensordataValueType[1]);
            }
            if ((textview_value1 != null) && (sensor.sensordataValue[1] != null)){
                textview_value1.setText(sensor.sensordataValue[1]);
            }
        }
    }

    @Override
    public void onBackPressed(){
        finish();
    }

    private void readPreferences() {
        preferences = new FeinstaubPreferences(this);
    }

}
