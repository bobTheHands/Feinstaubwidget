/**

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import android.util.JsonReader;
import java.io.IOException;
import java.util.ArrayList;

public class LuftDatenAPILocationStreamReader extends LuftDatenAPIStreamReader {

    ArrayList<Integer> knownlocations  = new ArrayList<Integer>();
    SensorlocationHandler handler;

    public LuftDatenAPILocationStreamReader(SensorlocationHandler handler){
        this.handler = handler;
    }

    @Override
    public Boolean saveLuftdatenAPIArray(JsonReader jsonReader) throws IOException, IllegalStateException{
        FullSensorDataSet dataset;
        int index = 0;
        int updatecounter = 0;
        int errors = 0;
        try {
            jsonReader.beginArray();
            while (jsonReader.hasNext()){
                updatecounter = updatecounter + 1;
                dataset = readLuftdatenAPISensorData(jsonReader);
                if (dataset != null){
                    if (isWantedSensorType(dataset)){
                        try {
                            Integer lid = Integer.valueOf(dataset.sensorId);
                            if (!knownlocations.contains(lid)) {
                                handler.addLocationDataSet(new LocationDataSet(dataset));
                                knownlocations.add(lid);
                                index = index + 1;
                            }
                        } catch (NumberFormatException e) {
                            // ignore entry
                        }
                    }
                } else {
                    errors = errors + 1;
                }
                if (updatecounter>124){
                    showgetLocationListFromAPIprogress(index,errors);
                    updatecounter = 0;
                }
            }
            jsonReader.endArray();
        } catch (IOException e){
            throw new IOException(JSONSTREAM_IO_ERROR, new Throwable(JSONSTREAM_ERROR_CAUSE));
        } catch (IllegalStateException e) {
            throw new IllegalStateException(JSONSTREAM_STATE_ERROR, new Throwable(JSONSTREAM_ERROR_CAUSE));
        }
        if (updatecounter>0){
            return true;
        } else {
            return false;
        }
    }

}
