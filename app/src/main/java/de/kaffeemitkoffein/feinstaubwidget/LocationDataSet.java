/**

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;

public class LocationDataSet implements Parcelable {

    String number = null;
    int type = 0;
    String source = null;
    Double latitude = null;
    Double longitude = null;
    Double altitude = null;
    Float distance = null;
    Float bearing = null;

    public LocationDataSet(){
        this.source = LuftDatenContentProvider.SensorLocationsDatabaseHelper.LOCATION_PROVIDER;
    }

    public LocationDataSet(SensorDataSet sensorDataSet){
        this.number = sensorDataSet.sensorId;
        this.type = sensorDataSet.getSensorType();
        this.latitude = sensorDataSet.getSensorLatitiude();
        this.longitude = sensorDataSet.getSensorLongitude();
        this.altitude = sensorDataSet.getSensorAltitude();
        this.source = LuftDatenContentProvider.SensorLocationsDatabaseHelper.LOCATION_PROVIDER;
    }

    public int getSensorNumber(){
        try {
            return Integer.valueOf(this.number);
        } catch (Exception e){
            return 0;
        }
    }

    public Location getLocation(){
        Location l = new Location(source);
        if (this.longitude != null)
            l.setLongitude(this.longitude);
        if (this.latitude != null)
            l.setLatitude(this.latitude);
        if (this.altitude != null)
            l.setAltitude(this.altitude);
        if (this.bearing != null)
            l.setBearing(this.bearing);
        Calendar calendar = Calendar.getInstance();
        l.setTime(calendar.getTimeInMillis());
        if (this.source!=null){
            l.setProvider(this.source);
        }
        return l;
    }

    /**
    Returns the distance in metres.
     **/

    public String getDistance(){
        return distance.toString();
    }

    /**
     * The following code makes this class parcelable.
     */

    @Override
    public void writeToParcel(Parcel parcel, int flags){
        parcel.writeString(number);
        parcel.writeInt(type);
        parcel.writeString(source);
        parcel.writeDouble(latitude);
        parcel.writeDouble(longitude);
        parcel.writeDouble(altitude);
        parcel.writeFloat(distance);
        parcel.writeFloat(bearing);
    }

    public LocationDataSet (Parcel parcel){
        number = parcel.readString();
        type = parcel.readInt();
        source = parcel.readString();
        latitude = parcel.readDouble();
        longitude = parcel.readDouble();
        altitude = parcel.readDouble();
        distance = parcel.readFloat();
        bearing = parcel.readFloat();
    }

    @Override
    public int describeContents(){
        return 0;
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator(){
        public LocationDataSet createFromParcel(Parcel parcel){
            return new LocationDataSet(parcel);
        }
        public LocationDataSet[] newArray(int i){
            return new LocationDataSet[i];
        }
    };

}
