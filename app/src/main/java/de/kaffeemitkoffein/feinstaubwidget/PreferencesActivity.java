/**

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import android.util.Log;

/**
 * The preferences are defined in XML in the preferences.xml file. Not
 * much to do here at the moment, as the preference activity is very
 * much a default one with no specials.
 */

public class PreferencesActivity extends PreferenceActivity {

    public static final String LOCATION_UPDATE_DATABASE_KEY="PREF_LOCATION_UPDATE_DATABASE";
    public static final String PREFERENCE_LOCATION_HINT="PREFERENCE_LOCATION_HINT";

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        addPreferencesFromResource(R.xml.preferences);
        Preference preference = findPreference(LOCATION_UPDATE_DATABASE_KEY);
        if (preference!=null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.getDefault());
            Long l_time = getSensorListLastUpdateTimeInMillis();
            String time = "-";
            String number = "-";
            if (l_time != 0) {
                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(l_time);
                time = sdf.format(c.getTimeInMillis());
                SensorlocationHandler sensorlocationHandler = new SensorlocationHandler(this);
                number = String.valueOf(sensorlocationHandler.getDataSetCount());
            }
            String pref_Summary = getResources().getString(R.string.preference_location_update_locations_summary) +
                    System.lineSeparator() +
                    getResources().getString(R.string.preference_location_update_locations_lastupdated) + " " +
                    time +
                    System.lineSeparator() +
                    getResources().getString(R.string.preference_location_update_locations_numberofsensors) + " " +
                    number;
            preference.setSummary(pref_Summary);
        }
        Preference location_gps = findPreference(FeinstaubPreferences.PREF_USE_LOCATION_PROVIDER_GPS);
        Preference location_network = findPreference(FeinstaubPreferences.PREF_USE_LOCATION_PROVIDER_NETWORK);
        Preference location_passive = findPreference(FeinstaubPreferences.PREF_USE_LOCATION_PROVIDER_PASSIVE);
        Preference location_hint = findPreference(PREFERENCE_LOCATION_HINT);
        FeinstaubLocationManager feinstaubLocationManager = new FeinstaubLocationManager(this);
        if (!feinstaubLocationManager.hasLocationPermission()){
            if (location_gps!=null){
                location_gps.setEnabled(false);
            }
            if (location_network!=null){
                location_network.setEnabled(false);
            }
            if (location_passive!=null){
                location_passive.setEnabled(false);
            }
            if (location_hint != null){
                location_hint.setSummary(this.getApplicationContext().getResources().getString(R.string.preference_location_hint_text2));
            }
        } else {
            if (location_gps!=null){
                location_gps.setEnabled(true);
            }
            if (location_network!=null){
                location_network.setEnabled(true);
            }
            if (location_passive!=null){
                location_passive.setEnabled(true);
            }
            if (location_hint != null){
                location_hint.setSummary(this.getApplicationContext().getResources().getString(R.string.preference_location_hint_text1));
            }
        }
    }

    private long getSensorListLastUpdateTimeInMillis(){
        FeinstaubPreferences feinstaubPreferences = new FeinstaubPreferences(this);
        return feinstaubPreferences.lastlocationlistupdatetime;
    }
}
