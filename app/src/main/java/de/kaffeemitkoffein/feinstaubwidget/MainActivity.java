/**

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.*;
import android.webkit.MimeTypeMap;
import android.widget.*;

import java.io.FileOutputStream;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import android.graphics.Bitmap;
import java.io.File;

public class MainActivity extends Activity {

    private Context context;

    /**
     * Field names to interact with the StaubInfo activity that displays a custom text
     * in a scrollable view.
     */

    private static final String DATA_TITLE="DATA_TITLE";
    private static final String DATA_TEXTRESOURCE="DATA_TEXTRESOURCE";
    private static final String DATA_BUTTONTEXT="DATA_BUTTONTEXT";

    /**
     * Bundle-id to identify the sensor number parameter.
     * Used to handle over a sensor number to the SensorInfo activity.
     */

    public static final String DATA_SENSORNUMBER="DATA_SENSORNUMBER";

    /**
     * This constant defines the size of the database by time. Entries older than that
     * are deleted.
     */

    private static final int DATABASE_TIMESPAN = 86400000;

    /**
     * This defines the time to display in the graphs.
     */

    private static final int GRAPH_TIMESPAN = DATABASE_TIMESPAN;

    /**
     * Constants for the visible views (mobile sensors or local sensors).
     */

    static final int CARDSELECTOR_MOBILE=0;
    static final int CARDSELECTOR_LOCAL=1;

    /**
     * Local variable holding the visible main view type (mobile sensors or local sensor).
     */

    private int visible_cardselector = CARDSELECTOR_MOBILE;

    private Boolean dataselectorposition = FeinstaubPreferences.DATASWITCHPOSITION_PARTICULATEMATTER;

    /**
     * Constant to identify the permission granted / not granted callbacks.
     */

    private static final int PERMISSION_CALLBACK_LOCATION = 97;
    private static final int PERMISSION_CALLBACK_STORAGE_SAVE = 98;
    private static final int PERMISSION_CALLBACK_STORAGE_SHARE = 99;
    private static final int PERMISSION_CALLBACK_NOACTION = 100;

    /**
     * String with the last *database* update time of the mobile view. This indicates the
     * last time when the API was queried.
     */

    private String table_lastupdatetext;

    /**
     * String with the last *location* update time. Please note that this time may be quite
     * old, e.g. when the device is not able to get a current location and/or the location
     * services have been turned off.
     */

    private String table_lastlocationtime;

    /**
     * A listener to listen for preference changes. Needs to be defined here
     * to prevent garbage collection by the OS.
     */

    private static SharedPreferences.OnSharedPreferenceChangeListener preferenceListener = null;

    /**
     * The local preferences.
     */

    private FeinstaubPreferences preferences;

    /**
     *  Turn verbose on/off for development.
     */

    private Boolean verbose = false;

    /**
     * View variables.
     */

    private ImageView graph1;
    private ImageView graph2;

    private Dialog introDialog;
    private AlertDialog shareHint;

    /**
     * Current location.
     */

    private FeinstaubLocationManager lm;
    private FeinstaubLocationListener loc_listener;
    private Location localLastKnownLocation;

    /**
     * This local variable is always set to true when the list of mobile sensors is
     * updated. This is necessary to block the luftObserver listener from updating
     * all the views because the SQL database has changed.
     *
     * This is mainly done for performance increase, as the views would be updated
     * too often.
     *
     * The listener is blocked in two cases:
     *  a) at app launch between onResume and onCreate, because onCreate already
     *     handles view updates.
     *  b) during the update of the list of next sensors from the API, as the
     *     getCloseDataSet class handles view updates by itself.
     */

    private Boolean updateCloseSensors_is_running = false;

    /**
     * This variable indicates if an update of the known sensors list is
     * currently running.
     */

    private Boolean readingAllLocationsList_is_running = false;

    /**
     * Local convenience variables that hold the view IDs of the table showing
     * the list of next close sensors.
     */

    private int[] locationtable_sensor_id = new int[6];
    private int[] locationtable_direction_id = new int[6];
    private int[] locationtable_distance_id = new int[6];
    private int[] locationtable_value1_id = new int[6];
    private int[] locationtable_value2_id = new int[6];
    private int[] locationtable_row_id = new int[6];

    /**
     * Local convenience sub to populate the arrays listed above.
     */

    private void setViewIdArrays() {
        locationtable_sensor_id[0] = R.id.main_nextsensors_id_1;
        locationtable_sensor_id[1] = R.id.main_nextsensors_id_2;
        locationtable_sensor_id[2] = R.id.main_nextsensors_id_3;
        locationtable_sensor_id[3] = R.id.main_nextsensors_id_4;
        locationtable_sensor_id[4] = R.id.main_nextsensors_id_5;
        locationtable_sensor_id[5] = R.id.main_nextsensors_id_6;

        locationtable_direction_id[0] = R.id.main_nextsensors_direction_1;
        locationtable_direction_id[1] = R.id.main_nextsensors_direction_2;
        locationtable_direction_id[2] = R.id.main_nextsensors_direction_3;
        locationtable_direction_id[3] = R.id.main_nextsensors_direction_4;
        locationtable_direction_id[4] = R.id.main_nextsensors_direction_5;
        locationtable_direction_id[5] = R.id.main_nextsensors_direction_6;

        locationtable_distance_id[0] = R.id.main_nextsensors_distance_1;
        locationtable_distance_id[1] = R.id.main_nextsensors_distance_2;
        locationtable_distance_id[2] = R.id.main_nextsensors_distance_3;
        locationtable_distance_id[3] = R.id.main_nextsensors_distance_4;
        locationtable_distance_id[4] = R.id.main_nextsensors_distance_5;
        locationtable_distance_id[5] = R.id.main_nextsensors_distance_6;

        locationtable_value1_id[0] = R.id.main_nextsensors_value1_1;
        locationtable_value1_id[1] = R.id.main_nextsensors_value1_2;
        locationtable_value1_id[2] = R.id.main_nextsensors_value1_3;
        locationtable_value1_id[3] = R.id.main_nextsensors_value1_4;
        locationtable_value1_id[4] = R.id.main_nextsensors_value1_5;
        locationtable_value1_id[5] = R.id.main_nextsensors_value1_6;

        locationtable_value2_id[0] = R.id.main_nextsensors_value2_1;
        locationtable_value2_id[1] = R.id.main_nextsensors_value2_2;
        locationtable_value2_id[2] = R.id.main_nextsensors_value2_3;
        locationtable_value2_id[3] = R.id.main_nextsensors_value2_4;
        locationtable_value2_id[4] = R.id.main_nextsensors_value2_5;
        locationtable_value2_id[5] = R.id.main_nextsensors_value2_6;

        locationtable_row_id[0] = R.id.main_closesensorrow_1;
        locationtable_row_id[1] = R.id.main_closesensorrow_2;
        locationtable_row_id[2] = R.id.main_closesensorrow_3;
        locationtable_row_id[3] = R.id.main_closesensorrow_4;
        locationtable_row_id[4] = R.id.main_closesensorrow_5;
        locationtable_row_id[5] = R.id.main_closesensorrow_6;
    }

    /**
     * Local variable if the local list of sensors should be reloaded. Necessary
     * for the interaction with the perferences screen.
     */

    private Boolean prompt_for_local_list_download = false;

    /**
     * Local variable if the local list of sensors should be updated. Necessary
     * for the interaction with the perferences screen.
     */

    private Boolean do_local_list_update = false;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (verbose)
            Log.v("FDW","Called onCreate()");
        setContentView(R.layout.activity_main);
        View mainView = getWindow().getDecorView().getRootView();
        /*
         * Read the app defaults and set the context variable.
         */
        context = this;
        readPreferences();
        /*
         * Inits the main selector between "mobile" and "local" and adds the respective
         * listeners to switch the views.
         */
        TextView mainselector_mobile = (TextView) findViewById(R.id.main_cardselector_text1);
        mainselector_mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchMainView(CARDSELECTOR_MOBILE);
            }
        });
        final TextView mainselector_local = (TextView) findViewById(R.id.main_cardselector_text2);
        mainselector_local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchMainView(CARDSELECTOR_LOCAL);
            }
        });
        /*
         * This is the switch to change the mobile view between fine particulate matter display
         * and temperature / humidity.
         */
        if (verbose)
            Log.v("FDW","Update close sensors is running: "+updateCloseSensors_is_running);
        final Switch mode_switch = (Switch) findViewById(R.id.main_nextsensors_dataswitch);
        mode_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!updateCloseSensors_is_running) {
                    // it is safe to switch as data is currently not updated from the api
                    updateCloseSensors(true,true);
                } else {
                    // the update is blocked and the switch is toggled back to it's last
                    // position because data is currently updated from the api. Allowing this
                    // would cause a running condition, because we would run at least
                    // two update processes in parallel.
                    mode_switch.setChecked(!mode_switch.isChecked());
                }
                storeDataSelectorPosition(mode_switch.isChecked());
                dataselectorposition = mode_switch.isChecked();
            }
        });
        /*
         * This catches a call from PreferencesActivity (call is defined in preferences.xml)
         * to re-load the database. This is triggered by resetting lastlocationupdate
         * to zero.
         *
         * As a result of this, selecting to re-load the database in the preferences screen
         * directly opens the dialog to download the database.
         *
         * This also requires to set the main selector to "mobile", as otherwise this view would
         * not be directly visible to the user.
         */

        Bundle extras = getIntent().getExtras();
        prompt_for_local_list_download = false;
        do_local_list_update = false;
        if (extras!=null){
            String action = extras.getString("ACTION");
            if (action != null){
                if (action.equals("LOADSENSORLOCATIONS")){
                    resetSensorLocationsLoadedFlag();
                    prompt_for_local_list_download = true;
                }
                if (action.equals("UPDATESENSORLOCATIONS")){
                    do_local_list_update = true;
                }
            }
        }
        mainView.post(new Runnable() {
                @Override
                public void run() {
                    setViewIdArrays();
                    visible_cardselector = getCardSelectorValue();
                    /*
                     * The main view is restored to the last known position (mobile or local),
                     * UNLESS the list with the sensor locations has to be reloaded the user
                     * is prompted to download it. In this case, the main view is
                     * set to CARDSELECTOR_MOBILE.
                     */
                    if ((!prompt_for_local_list_download) && (!do_local_list_update)){
                        switchMainView(visible_cardselector);
                    } else {
                        switchMainView(CARDSELECTOR_MOBILE);
                    }
                    /*
                     * Read the dataselector position and assign it to the switch.
                     */
                    dataselectorposition = getDataSelectorPosition();
                    Switch dataswitch = (Switch) findViewById(R.id.main_nextsensors_dataswitch);
                    if (dataswitch!=null){
                        dataswitch.setChecked(dataselectorposition);
                    }
                    /*
                     * The user is prompted to download the sensor locations if necessary.
                     */
                    if (verbose)
                        Log.v("FSW","preferences.lastlocationlistupdatetime: "+preferences.lastlocationlistupdatetime);
                    if (preferences.lastlocationlistupdatetime == 0){
                        LinearLayout initial_location_info = (LinearLayout) findViewById(R.id.main_location_init_dialog_container);
                        initial_location_info.setVisibility(LinearLayout.VISIBLE);
                        initial_location_info.invalidate();
                        Button initial_location_loadbutton = (Button) findViewById(R.id.location_init_dialog_download_button);
                        initial_location_loadbutton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                updateSensorLocationsList(false,true);
                                showDownloadWarning();
                            }
                        });
                    }
                    if (do_local_list_update){
                        updateSensorLocationsList(false,false);
                        showDownloadWarning();
                    }
                    /*
                    /*
                     * Garbage-collect old sensor data.
                     */
                    cleanDatabase();
                    /*
                     * Add the listeners for the sensor info.
                     */
                    graph1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(context, SensorInfo.class);
                            i.putExtra(DATA_SENSORNUMBER, preferences.sensor1_number);
                            startActivity(i);
                        }
                    });
                    graph2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(context, SensorInfo.class);
                            i.putExtra(DATA_SENSORNUMBER, preferences.sensor2_number);
                            startActivity(i);
                        }
                    });
                    /*
                     * release blocked updates.
                     */
                    releaseUpdateBlocker();
                    /*
                     * We ask for the location permission, but only if:
                     * - the mobile view is visible,
                     * - the user has not been redirected here from the settings to update
                    */
                    if ((visible_cardselector==CARDSELECTOR_MOBILE) && (!prompt_for_local_list_download) && (!do_local_list_update)) {

                        hasLocationPermission(true);
                    }
                }
            });
    }

    @Override
    protected void onPause(){
        if (verbose)
            Log.v("FDW","Called onPause()");
        preferences.sharedPreferences.unregisterOnSharedPreferenceChangeListener(preferenceListener);
        /*
         * Store that currently visible main menu (mobile or local).
         */
        storeCardSelectorValue(visible_cardselector);
        /*
         * If any info dialog is open, close it.
         */
        if (introDialog != null){
            if (introDialog.isShowing())
                introDialog.dismiss();
        }
        if (shareHint != null){
            if (shareHint.isShowing()){
                shareHint.dismiss();
            }
        }
        super.onPause();
    }

    @Override
    protected void onResume(){
        super.onResume();
        if (verbose)
            Log.v("FDW","Called onResume()");
        readPreferences();
        /*
         * Restore the last known visible main menu (mobile or local).
         */
        visible_cardselector = getCardSelectorValue();
        /*
         * Restore data switch position.
         */
        dataselectorposition = getDataSelectorPosition();
        Switch dataswitch = (Switch) findViewById(R.id.main_nextsensors_dataswitch);
        if (dataswitch != null){
            dataswitch.setChecked(dataselectorposition);
        }
        /*
        * Sets up a listener to listen for preference changes. This ensures that
        * the displays (and widgets) get updated, when:
        *  a) the database of sensors was downloaded successfully
        *  b) the local sensors were changed
        *  c) GPS settings were changed
        *  d) display settings were changed
        *
        *  Note: preferenceListener needs to be global to prevent garbage-
        *  collection of this listener.
        */
        registerPreferenceListener();
        updateDataDisplays();
        /*
         * Finally, update the mobile view with data. Try first, if local data
         * satisfies the needs, load new data otherwise.
         */
        updateCloseSensors(true,false);
        getLocalSensorDataFromAPI();
    }

    /**
     * Some field names to parcel the currently displayed data.
     */

    public final static String NEXTSENSORS_DATA = "NEXTSENSORS_DATA";
    public final static String NEXTSENSORS_LOCATION = "NEXTSENSORS_LOCATION";
    public final static String LASTTABLEUPDATE = "LASTTABLEUPDATE";
    public final static String LASTLOCUPDATE = "LASTLOCUPDATE";
    public final static String TABLEUPDATERUNNING = "TABLEUPDATERUNNING";
    public final static String LOCALLASTKNOWNLOCATION = "LOCALLASTKNOWNLOCATION";

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
        if (verbose)
            Log.v("FDW","Called onSaveInstanceState()");

        /*
         * Parcel the currently displayed/known sensor data to restore it during
         * onCreate. This is mainly done to speed up the UI display when the
         * screen is rotated.
         */
        savedInstanceState.putParcelableArrayList(NEXTSENSORS_DATA,nextSensors_data);
        savedInstanceState.putParcelableArrayList(NEXTSENSORS_LOCATION,nextSensors_location);
        TextView v = (TextView) findViewById(R.id.main_nextsensors_lastupdate_text);
        if (v!=null) {
            savedInstanceState.putString(LASTTABLEUPDATE, String.valueOf(v.getText()));
        }
        TextView x = (TextView) findViewById(R.id.main_nextsensors_devicelocation_text);
        if (x!=null) {
            savedInstanceState.putString(LASTLOCUPDATE, String.valueOf(x.getText()));
        }
        savedInstanceState.putBoolean(TABLEUPDATERUNNING,readingAllLocationsList_is_running);
        savedInstanceState.putParcelable(LOCALLASTKNOWNLOCATION,localLastKnownLocation);
    }

    private void restoreLocalVariablesFromInstanceState(Bundle restoredInstanceState){
        nextSensors_data                   = restoredInstanceState.getParcelableArrayList(NEXTSENSORS_DATA);
        nextSensors_location               = restoredInstanceState.getParcelableArrayList(NEXTSENSORS_LOCATION);
        table_lastupdatetext               = restoredInstanceState.getString(LASTTABLEUPDATE);
        table_lastlocationtime             = restoredInstanceState.getString(LASTLOCUPDATE);
        readingAllLocationsList_is_running = restoredInstanceState.getBoolean(TABLEUPDATERUNNING);
        localLastKnownLocation             = restoredInstanceState.getParcelable(LOCALLASTKNOWNLOCATION);
    }


    @Override
    public void onRestoreInstanceState(Bundle restoredInstanceState){
        super.onRestoreInstanceState(restoredInstanceState);
        if (verbose)
            Log.v("FDW","Called onRestoreInstanceState()");

        /*
         * Retrieve the last displayed/known sensor data to restore the displays. This
         * is mainly done to speed up the UI display when the screen is roated.
         */
        restoreLocalVariablesFromInstanceState(restoredInstanceState);
        View mainView = getWindow().getDecorView().getRootView();
        mainView.post(new Runnable() {
            @Override
            public void run() {
                updateCloseSensorsMap();
            }
        });
    }

    public void registerPreferenceListener(){
        if (preferenceListener==null){
            preferenceListener = new SharedPreferences.OnSharedPreferenceChangeListener(){
                @Override
                public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
                    readPreferences();
                    updateDataDisplays();
                    registerToLocationUpdates();
                }
            };
            preferences.sharedPreferences.registerOnSharedPreferenceChangeListener(preferenceListener);
        }
    }

    /**
     * Checks for location permissions. Takes into account the different handling of
     * permissions after API 23, where permissions have to be granted at runtime.
     *
     * Prior to API 23, permissions are granted during app install. So for devices
     * running API 23 or earlier, the app can assume that all necessary permissions
     * are granted.
     *
     * @return
     */

    private boolean hasLocationPermission(Boolean request){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            // handle runtime permissions only if android is >= Marshmellow api 23
            if (this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                if (request){
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_CALLBACK_LOCATION);
                    // at this moment, we request the permission and leave with a negative result. The user needs to try saving again after permission was granted.
                }
                // permission not granted
                return false;
            }
            else
            {
                // permission is granted, ok
                return true;
            }
        }
        else
        {
            // before api 23, permissions are always granted, so everything is ok
            return true;
        }
    }

    /**
     * Callback to catch the granted or denied location permission.
     *
     * @param permRequestCode
     * @param perms
     * @param grantRes
     */

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int permRequestCode, String perms[], int[] grantRes){
        Boolean hasLocationPermission = false;
        Boolean hasReadAccess  = false;
        Boolean hasWriteAccess = false;
        for (int i=0; i<grantRes.length; i++){
            if ((perms[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE)) && (grantRes[i]==PackageManager.PERMISSION_GRANTED)){
                hasReadAccess = true;
            }
            if ((perms[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) && (grantRes[i]==PackageManager.PERMISSION_GRANTED)){
                hasWriteAccess = true;
            }
            if ((perms[i].equals(Manifest.permission.ACCESS_FINE_LOCATION)) && (grantRes[i]==PackageManager.PERMISSION_GRANTED)){
                hasLocationPermission = true;
            }
        }
        if (permRequestCode == PERMISSION_CALLBACK_LOCATION){
                if (hasLocationPermission){
                    //
                    // Define the actions once permissions are granted.
                    //
                    registerToLocationUpdates();
                } else {
                    if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)){
                        showPermissionsRationale();
                    }
                }
            }
        if (permRequestCode == PERMISSION_CALLBACK_STORAGE_SAVE){
            if ((hasReadAccess)&&(hasWriteAccess)){
                saveBitmapToStorage(getShareDataBitmap(visible_cardselector),PERMISSION_CALLBACK_NOACTION,true);
            }
        }
        if (permRequestCode == PERMISSION_CALLBACK_STORAGE_SHARE){
            if ((hasReadAccess)&&(hasWriteAccess)){
                shareVisibleData(PERMISSION_CALLBACK_NOACTION);
            }
        }
    }

    /**
     * Shows an explanation why the location permission is needed if it was
     * not granted by the user.
     */

    private void showPermissionsRationale(){
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialogpermrationale);
        dialog.setTitle(R.string.permissions_title);
        dialog.setCancelable(true);
        TextView tv = (TextView) dialog.findViewById(R.id.permrationale_text);
        tv.setText(getResources().getString(R.string.permissions_rationale_1)+
                System.lineSeparator()+
                getResources().getString(R.string.permissions_rationale_2)+
                System.lineSeparator()+
                getResources().getString(R.string.permissions_rationale_3)+
                System.lineSeparator()+
                getResources().getString(R.string.permissions_rationale_4)+
                System.lineSeparator()+
                getResources().getString(R.string.permissions_rationale_5)+
                System.lineSeparator()+
                getResources().getString(R.string.permissions_rationale_6)+
                System.lineSeparator()+
                getResources().getString(R.string.permissions_rationale_7)+
                System.lineSeparator()+
                getResources().getString(R.string.permissions_rationale_8)+
                System.lineSeparator());
        Button permok_button = (Button) dialog.findViewById(R.id.permok_button);
        permok_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /**
     * Populates the action bar.
     *
     * @param menu
     * @return
     */

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.mainactivity,menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Defines the actions for the selected menu items in the action bar.
     *
     * @param mi
     * @return
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem mi){
        int item_id = mi.getItemId();
      if (item_id == R.id.menu_settings) {
            Intent i = new Intent(this,PreferencesActivity.class);
            startActivity(i);
            return true;
        }
        if (item_id == R.id.menu_refresh) {
            updateSensorDataFromAPI();
            return true;
        }
        if (item_id == R.id.menu_save) {
            saveOrShareCurrentVisibleData(ACTION_SAVE);
        }
        if (item_id == R.id.menu_share) {
            saveOrShareCurrentVisibleData(ACTION_SHARE);
        }
        if (item_id==R.id.menu_licence) {
            Intent i = new Intent(this, StaubInfo.class);
            i.putExtra(DATA_TITLE, getResources().getString(R.string.license_title));
            i.putExtra(DATA_TEXTRESOURCE, "license");
            i.putExtra(DATA_BUTTONTEXT,getResources().getString(R.string.license_button_text));
            startActivity(i);
            return true;
        }
        if (item_id == R.id.menu_about) {
            showIntroDialog();
            return true;
        }
        if (item_id==R.id.menu_help) {
            Intent i = new Intent(this, StaubInfo.class);
            i.putExtra(DATA_TITLE, getResources().getString(R.string.help_title));
            i.putExtra(DATA_TEXTRESOURCE, "help");
            i.putExtra(DATA_BUTTONTEXT,getResources().getString(R.string.license_button_text));
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(mi);
    }

    /**
     * Switches between the two main views: mobile and local.
     *
     * @param i
     */

    private void switchMainView(int i){
        final LinearLayout mobilecontainer = (LinearLayout) findViewById(R.id.main_mobile_container);
        final LinearLayout localcontainer = (LinearLayout) findViewById(R.id.main_local_container);
        final View mobile_line = (View) findViewById(R.id.main_cardselector_line1);
        final View local_line = (View) findViewById(R.id.main_cardselector_line2);
        if (i==CARDSELECTOR_MOBILE){
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mobilecontainer.setVisibility(View.VISIBLE);
                    mobilecontainer.invalidate();
                    localcontainer.setVisibility(View.GONE);
                    localcontainer.invalidate();
                    mobile_line.setVisibility(View.VISIBLE);
                    mobile_line.invalidate();
                    local_line.setVisibility(View.INVISIBLE);
                    local_line.invalidate();
                }
            });
        } else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    localcontainer.setVisibility(View.VISIBLE);
                    localcontainer.invalidate();
                    mobilecontainer.setVisibility(View.GONE);
                    mobilecontainer.invalidate();
                    mobile_line.setVisibility(View.INVISIBLE);
                    mobile_line.invalidate();
                    local_line.setVisibility(View.VISIBLE);
                    local_line.invalidate();
                    displayLocalDataset();
                    drawGraphs();
                }
            });
        }
        visible_cardselector = i;
    }

    /**
     * Calls the sensor setup activity.
     */

    private void call_SensorAPILogin(){
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
    }


    private void setUpdateBlocker(final int max){
        updateCloseSensors_is_running = true;
        Runnable r = new Runnable(){
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void run(){
                ProgressBar progressBar = (ProgressBar) findViewById(R.id.main_updatebar);
                if (progressBar!=null){
                    Drawable d = progressBar.getIndeterminateDrawable().mutate();
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        d.setColorFilter(getResources().getColor(R.color.primaryLightColor,context.getTheme()),PorterDuff.Mode.SCREEN);
                    } else {
                        d.setColorFilter(getResources().getColor(R.color.primaryLightColor),PorterDuff.Mode.SCREEN);
                    }
                    progressBar.setIndeterminateDrawable(d);
                    Drawable d2 = progressBar.getProgressDrawable().mutate();
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        d2.setColorFilter(getResources().getColor(R.color.primaryLightColor,context.getTheme()),PorterDuff.Mode.SCREEN);
                    } else {
                        d2.setColorFilter(getResources().getColor(R.color.primaryLightColor),PorterDuff.Mode.SCREEN);
                    }
                    progressBar.setProgressDrawable(d2);
                    if (max != 0){
                        progressBar.setMax(max);
                        progressBar.setProgress(0);
                        progressBar.setIndeterminate(false);
                    } else {
                        progressBar.setIndeterminate(true);

                    }
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.invalidate();
                }
            }
        };
        runOnUiThread(r);
        if (verbose)
            Log.v("FSB","setUpdateBlocker called. New calls to update from API are now blocked.");
    }

    private void updateProgressBar(final int progress){
        Runnable r = new Runnable() {
            @Override
            public void run() {
                ProgressBar progressBar = (ProgressBar) findViewById(R.id.main_updatebar);
                if (progressBar != null) {
                    if ((!progressBar.isIndeterminate()) && (progressBar.getVisibility()==View.VISIBLE)){
                        progressBar.setProgress(progress);
                        progressBar.invalidate();
                    }
                }
            }
        };
        runOnUiThread(r);
    }

    private void releaseUpdateBlocker(){
        updateCloseSensors_is_running = false;
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.main_updatebar);
        if (progressBar!=null){
            if (progressBar.getVisibility() == View.VISIBLE){
                progressBar.setVisibility(View.GONE);
            }
        }
        if (verbose)
            Log.v("FSB","releaseUpdateBlocker called. New calls to update from API are allowed again.");
    }

    /**
     * Updates all displays including all widgets with known, available data, either present
     * in variables or in the local data base.
     *
     * Calling this will not call the API.
     */


    private void updateDataDisplays(){
        readPreferences();
        displayLocalDataset();
        drawGraphs();
        updateCloseSensorsFromLocalVariables();
        updateCloseSensorsMap();
        checkForWarning();
        checkForReferenceDisplay();
    }


    public static final int REPFRESH_ALL_WIDGETS = 0;
    public static final int REPFRESH_LOCAL_WIDGETS = 1;
    public static final int REPFRESH_CLOSE_WIDGETS = 2;

    /**
     * Updates widget instances by category.
     *
     * Following this update call, the widgets display present data only and do not
     * query for new data.
     *
     * The intention of this sub is to pass on now data obtained from the main app
     * to the widgets, so that they also display the most current data.
     */

    public void refreshWidgets(int update_type){
        if ((update_type==REPFRESH_ALL_WIDGETS) || (update_type == REPFRESH_LOCAL_WIDGETS)){
            // refresh small widgets
            Intent widget_small = new Intent(this,StaubWidget.class);
            widget_small.setAction(StaubWidget.WIDGET_CUSTOM_REFRESH_ACTION);
            sendBroadcast(widget_small);
            // refresh big widgets
            Intent widget_big = new Intent(this,WidgetBig.class);
            widget_big.setAction(StaubWidget.WIDGET_CUSTOM_REFRESH_ACTION);
            sendBroadcast(widget_big);
        }
        if ((update_type==REPFRESH_ALL_WIDGETS) || (update_type == REPFRESH_CLOSE_WIDGETS)){
            // refresh close widgets
            Intent widget_close = new Intent(this,WidgetClose.class);
            widget_close.setAction(StaubWidget.WIDGET_CUSTOM_REFRESH_ACTION);
            /*
             * Put known location to intent so that the close widgets get it. This avoids
             * unnecessary calls of the location api by the widgets.
             */
            if (localLastKnownLocation != null){
                Bundle b = new Bundle();
                b.putParcelable(FeinstaubLocationManager.KEY_LOCATION_CHANGED,localLastKnownLocation);
                b.putParcelableArrayList(NEXTSENSORS_LOCATION,nextSensors_location);
                b.putParcelableArrayList(NEXTSENSORS_DATA,nextSensors_data);
                widget_close.putExtras(b);
            }
            sendBroadcast(widget_close);
        }
    }

    /**
     * Convenience method to get a string with the time from a location.
     */

    private String getLocalLocationTimeString(Location l){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(l.getTime());
        SimpleDateFormat simpleDateFormat_target = new SimpleDateFormat("dd.MM. HH:mm", Locale.getDefault());
        try{
            return simpleDateFormat_target.format(calendar.getTimeInMillis());
        } catch (Exception e){
            return "";
        }
    }

    /**
     * Convenience method to get a string with the current date & time.
     */

    private String getCurrentTimeString(){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat_target = new SimpleDateFormat("dd.MM. HH:mm", Locale.getDefault());
        try{
            return simpleDateFormat_target.format(calendar.getTimeInMillis());
        } catch (Exception e){
            return "";
        }
    }

    /**
     * Checks if the known data in the local variables is considered too old to
     * display to the user.
     *
     * @return  #true if the data is too old or there is no data at all. Otherwise
     *          returns false.
     */

    private boolean mobileDataTooOld() {
        if (nextSensors_data == null){
            return true;
        }
        if (nextSensors_data.size() == 0) {
            return true;
        }
        long time = nextSensors_data.get(0).getTimestampLocalMillis();
        for (int i = 0; i < nextSensors_data.size(); i++) {
            if (nextSensors_data.get(i).getTimestampLocalMillis() > time) {
                time = nextSensors_data.get(i).getTimestampLocalMillis();
            }
        }
        Calendar c = Calendar.getInstance();
        if (c.getTimeInMillis() - time < StaubWidget.WIDGET_CONN_RETRY_DELAY/2) {
            return false;
        }
        return true;
    }

    /**
     * Displays the status of the location service if necessary.
     */

    private void displayLocationServiceStatus(){
        if (lm == null){
            registerToLocationUpdates();
        }
        if (lm == null)
            return;
        LinearLayout container = (LinearLayout) findViewById(R.id.main_nextsensors_nolocation_hint_container);
        ImageView icon = (ImageView) findViewById(R.id.main_nextsensors_nolocation_hint_icon);
        TextView hint = (TextView) findViewById(R.id.main_nextsensors_nolocation_hint_text);
        if ((container!=null) && (hint != null) && (icon != null)){
            if (!lm.isProviderEnabled(preferences)){
                hint.setText(getResources().getString(R.string.main_location_devicestatus_nolocationhint_service_off));
                container.setVisibility(LinearLayout.VISIBLE);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    container.setBackgroundColor(this.getResources().getColor(R.color.secondaryDarkColor,getTheme()));
                } else {
                    container.setBackgroundColor(this.getResources().getColor(R.color.secondaryDarkColor));
                }
                container.invalidate();
                icon.setVisibility(ImageView.VISIBLE);
                icon.setImageResource(R.mipmap.ic_location_off_white_24dp);
                icon.invalidate();
                hint.setVisibility(TextView.VISIBLE);
                hint.invalidate();
            } else {
                if (localLastKnownLocation==null){
                    hint.setText(getResources().getString(R.string.main_location_devicestatus_nolocationhint_service_on));
                    container.setVisibility(LinearLayout.VISIBLE);
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        container.setBackgroundColor(this.getResources().getColor(R.color.secondaryDarkColor,getTheme()));
                    } else {
                        container.setBackgroundColor(this.getResources().getColor(R.color.secondaryDarkColor));
                    }
                    container.invalidate();
                    icon.setVisibility(ImageView.VISIBLE);
                    icon.setImageResource(R.mipmap.ic_location_on_white_24dp);
                    icon.invalidate();
                    hint.setVisibility(TextView.VISIBLE);
                    hint.invalidate();
                    registerToLocationUpdates();
                } else {
                    container.setVisibility(LinearLayout.GONE);
                    container.invalidate();
                }
            }
        }
    }

    /**
     * Update the table with the next closest sensors.
     *
     * @param loadnewdata true = update from API, false = update from local database
     */

    private void updateCloseSensors(Boolean loadnewdata, Boolean forceupdate){
        /*
         * Gets the last known location of the device.
         */
        if (hasLocationPermission(false)){
            getLastKnownLocationIfNotKnown(this);
            updateCloseSensors(localLastKnownLocation,loadnewdata,forceupdate);
        } else {
            /*
             * Sets the location to the 1st sensor if this is the desired behavior.
             */
            if (preferences == null){
                readPreferences();
            }
            if (preferences.use_local_sensor_location_as_fallback){
                SensorlocationHandler sensorlocationHandler = new SensorlocationHandler(this);
                localLastKnownLocation = sensorlocationHandler.getLocationFromSensor(preferences.sensor1_number);
                updateCloseSensors(localLastKnownLocation,loadnewdata,forceupdate);
            }
        }
    }

    /**
     * Updates the close sensors view with new data.
     *
     * The data is updated from the local database; the API is only called if the data from the
     * local database is too old.
     *
     * "too old" is defined by the WIDGET_CONN_RETRY_DELAY, which is 5 minutes.
     *
     * This ensures that both the main app and the widget reasonably share data and do not call
     * the API too often.
     *
     * @param l             last known location
     * @param loadnewdata   set to #true if the API should be called if data is too old. #false
     *                      suppresses API calls.
     * @param forceupdate   set to #true to force update regardless of local data age.
     *
     */

    private void updateCloseSensors(Location l, Boolean loadnewdata, Boolean forceupdate){
        if (lm == null){
            lm = new FeinstaubLocationManager(this);
        }
        if (l!=null){
            localLastKnownLocation = l;
        }
        if (verbose)
            Log.v("MAIN","updateCloseSensors was called.");
        displayLocationServiceStatus();
        TextView own_location_text = (TextView) findViewById(R.id.main_nextsensors_devicelocation_text);
        if ((l != null) && (preferences.lastlocationlistupdatetime != 0)){
            DecimalFormat decimalFormat = new DecimalFormat();
            decimalFormat.applyPattern("###.###");
            String lat_string  = decimalFormat.format(l.getLatitude());
            String long_string = decimalFormat.format(l.getLongitude());
            own_location_text.setText(getResources().getString(R.string.main_location_devicestatus_position_text)+ " " +
                    getResources().getString(R.string.main_location_devicestatus_position_lat)+" "+lat_string + " " +
                    getResources().getString(R.string.main_location_devicestatus_position_long)+" "+long_string + " "+
                    "("+getLocalLocationTimeString(l)+" "+lm.getLocationSourceChar(l)+")");
            /*
             * Read data from API if:
             * a) forced update = true
             * b) load new data is allowed & data is too old
             */
            if (((loadnewdata) && (mobileDataTooOld())) || (forceupdate)) {
                // load new data from the API
                clearCloseSensorsLocalVariables();
                updateCloseSensorsDataFromAPI(l);
            } else {
                /*
                 * otherwise, display known data.
                 */
                clearCloseSensorsLocalVariables();
                updateCloseSensorsFromPresentData(l);
            }
        } else {
            own_location_text.setText(getResources().getString(R.string.main_location_devicestatus_notavaiable));
        }
    }

    /**
     * Updates the map (fine particulate matter) or the chart (temperature/humidity) of the mobile view. The
     * type of data is handled by the DisplayManager, which produces the correct image.
     */

    public void updateCloseSensorsMap(){
        ImageView i = (ImageView) findViewById(R.id.main_closesensors_graph);
        if (i != null){
            if ((nextSensors_data != null) && (nextSensors_location != null)){
               if (nextSensors_location.size()>0){
                  DisplayManager dm = new DisplayManager();
                  dm.drawCloseSensorsMap(nextSensors_location,nextSensors_data,i,preferences.large_circles);
               }
           }
        }
    }

    /**
     * Updates the main app with data from the API. Updates the close sensors
     * or the local sensors, depending on what is visible.
     */

    public void updateSensorDataFromAPI(){
        if (visible_cardselector == CARDSELECTOR_LOCAL){
            getLocalSensorDataFromAPI();
        } else {
            if (hasLocationPermission(true)){
                // forceupdate = false, because we do not update from API if the data is very new.
                updateCloseSensors(true,false);
                requestSingleLocationUpdate();
            } else {
                /*
                 * Gets location from the 1st sensor if this is the desired behavior.
                 */
                if (preferences.use_local_sensor_location_as_fallback){
                    updateCloseSensors(true,false);
                }
            }
        }
    }

    /**
     * Shows the dialog to download the database with all the sensor locations. Please note that this
     * is not really a dialog but a view that simply gets invisible when not needed.
     */

    private void showDownloadWarning(){
        LinearLayout initial_location_info = (LinearLayout) findViewById(R.id.main_location_init_dialog_container);
        initial_location_info.setVisibility(LinearLayout.VISIBLE);
        ProgressBar spinner = (ProgressBar) findViewById(R.id.main_location_init_dialog_spinner);
        spinner.setVisibility(View.VISIBLE);
        spinner.invalidate();
        TextView location_info_text = (TextView) findViewById(R.id.location_init_dialog_download_text);
        location_info_text.setText(context.getResources().getString(R.string.main_location_data_keepopen));
        initial_location_info.invalidate();
        Button initial_location_loadbutton = (Button) findViewById(R.id.location_init_dialog_download_button);
        initial_location_loadbutton.setVisibility(View.GONE);
    }

    /**
     * Checks if the dialog (see above) can be closed and closes it when necessary.
     *
     * In particular, it gets invisible when there is a local database with location data.
     */

    private void checkForWarning(){
        LinearLayout initial_location_info = (LinearLayout) findViewById(R.id.main_location_init_dialog_container);
        ProgressBar progressBar            = (ProgressBar) findViewById(R.id.main_location_init_dialog_spinner);
        TextView location_info_text        = (TextView) findViewById(R.id.location_init_dialog_download_text);
        if ((preferences.lastlocationlistupdatetime !=0 ) && (initial_location_info.getVisibility() != View.GONE)){
            initial_location_info.setVisibility(View.GONE);
            initial_location_info.invalidate();
        } else if (readingAllLocationsList_is_running){
            showDownloadWarning();
        }
    }

    /**
     * Convenience call to reset the PREF_LOCATIONUPDATE to zero. This lets the app now that the
     * list of sensor locations has to be reloaded.
     * In other words: this call invalidates the list of known sensor locations and forces a
     * reload.
     */

    private void resetSensorLocationsLoadedFlag(){
        /*
         * .commit() is used (synchronous) instead of .apply() to prevent any running conditions
         * regarding the state of the list of known sensors.
         * In particular, this forces all app components to stop using the invalidated list of
         * known sensor locations immediately.
          */
        preferences.commitPreference(preferences.PREF_LOCATIONUPDATE,new Long(0));
    }

    /**
     * Downloads all known sensor locations form the API and stores them locally.
     *
     * @param onlyifempty
     * true  = download only if no data present,
     * false = always download
     *
     * @param delete_list
     * true  = delete the present list before loading. This removes all known sensors
     *         before looking for new ones.
     * false = add new sensors to the existing data base
     *
     *
     * @return
     * Returns true if the download started. Please note: this does not indicate *success* of
     * downloading and parsing the data, but only that the process was started.
     */

    private Boolean updateSensorLocationsList(Boolean onlyifempty, Boolean delete_list){
        if ((onlyifempty) && (preferences.lastlocationlistupdatetime != 0)) {
            return false;
        }
        SensorlocationHandler sldh = new SensorlocationHandler(context);
        if (delete_list) {
        /*
           Set the flag in the settings that here is no data. This is done to prevent
           broken / incomplete databases.
        */
            resetSensorLocationsLoadedFlag();
        /*
           Tries to delete the data first.
         */
            SensorlocationHandler sensorlocationHandler = new SensorlocationHandler(this);
            sensorlocationHandler.deleteSensorDataBase();
            // this.deleteDatabase(LuftDatenContentProvider.SensorLocationsDatabaseHelper.DATABASE_NAME);
        }
        /*
           Defines a runnable that will run after the processing of new sensors was
           sucessful. This will then look for new data and promptly update the
           mobile screen.
         */
        Runnable r_success = new Runnable() {
            @Override
            public void run() {
                Calendar c = Calendar.getInstance();
                preferences.commitPreference(preferences.PREF_LOCATIONUPDATE,new Long(c.getTimeInMillis()));
                releaseUpdateBlocker();
                // Allow screen rotation within this app again
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

                Toast.makeText(context,getResources().getString(R.string.main_location_data_success),Toast.LENGTH_LONG).show();
                updateCloseSensors(true,true);
            }
        };
        /*
           Defines a runnable that will run after the processing of new sensors
           failed.
         */
        Runnable r_error = new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context,getResources().getString(R.string.main_location_data_failed),Toast.LENGTH_LONG).show();
                releaseUpdateBlocker();
                // Allow screen rotation within this app again
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            }
        };
        /*
           Starts download and processing of the sensor locations.
         */
        int[] supported_sensortypes = {SensorDataSet.PARTICULATE_SENSOR, SensorDataSet.TEMPERATURE_SENSOR};
        try {
            sldh.setFetchList_positiveresultMessage(getResources().getString(R.string.main_location_data_success));
            sldh.setFetchList_negativeresultMessage(getResources().getString(R.string.main_location_data_failed));
            sldh.setWarning_container_view(findViewById(R.id.main_location_init_dialog_container));
            setUpdateBlocker(0);
            // Lock screen rotation during list download & processing.
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
            sldh.getLocationDataFromAPI(this,supported_sensortypes,!delete_list, r_error,r_success,(TextView) findViewById(R.id.location_init_dialog_progress_text));
        } catch (Exception e){
            releaseUpdateBlocker();
            return false;
        }
        return true;
    }


    /**
     * Deletes old data entries that are older than DATABASE_TIMESPAN from now.
     *
     * Please note: the CardHandler can do this synchronously in the main thread or
     * asynchronously.
     *
     * The synchronous delete is recommended for the widget, the async delete is
     * recommended to the main app.
     */

    private void cleanDatabase(){
        Calendar calendar = Calendar.getInstance();
        Long cut_date_in_millis = calendar.getTimeInMillis()- DATABASE_TIMESPAN; // -24h in UTC
        Date cut_date = new Date(cut_date_in_millis);
        CardHandler ch = new CardHandler(this);
        ch.deleteOldEntries(cut_date,true);
    }

    /**
     * Displays the text values of the local sensor.
     */

    private void displayLocalDataset(){
        CardHandler ch = new CardHandler(this);
        SensorDataSet sensor1_data = ch.getLatestDataSet(preferences.sensor1_number);
        SensorDataSet sensor2_data = ch.getLatestDataSet(preferences.sensor2_number);
        DisplayManager dsm = new DisplayManager();
        if (sensor1_data != null){
            dsm.displayCurrentDataset(context,
                    sensor1_data,
                    (TextView) findViewById(R.id.main_sensor1_value),
                    (TextView) findViewById(R.id.main_sensor1_timestamp),
                    (TextView) findViewById(R.id.main_sensor1_timestamp_value),
                    (TextView) findViewById(R.id.main_sensor1_data1_label),
                    (TextView) findViewById(R.id.main_sensor1_data2_label),
                    (TextView) findViewById(R.id.main_sensor1_data1_value),
                    (TextView) findViewById(R.id.main_sensor1_data2_value),
                    (TextView) findViewById(R.id.main_sensor1_location_country_value),
                    (TextView) findViewById(R.id.main_sensor1_location_long_value),
                    (TextView) findViewById(R.id.main_sensor1_location_lat_value),
                    (TextView) findViewById(R.id.main_sensor1_location_alt_value));
        }
        if (sensor2_data != null){
            dsm.displayCurrentDataset(context,
                    sensor2_data,
                    (TextView) findViewById(R.id.main_sensor2_value),
                    (TextView) findViewById(R.id.main_sensor2_timestamp),
                    (TextView) findViewById(R.id.main_sensor2_timestamp_value),
                    (TextView) findViewById(R.id.main_sensor2_data1_label),
                    (TextView) findViewById(R.id.main_sensor2_data2_label),
                    (TextView) findViewById(R.id.main_sensor2_data1_value),
                    (TextView) findViewById(R.id.main_sensor2_data2_value),
                    (TextView) findViewById(R.id.main_sensor2_location_country_value),
                    (TextView) findViewById(R.id.main_sensor2_location_long_value),
                    (TextView) findViewById(R.id.main_sensor2_location_lat_value),
                    (TextView) findViewById(R.id.main_sensor2_location_alt_value));
        }
    }

    /**
     * Handles drawing the graphs of both local sensors.
     *
     * @param sensor1
     * @param sensor2
     * @param image1
     * @param image2
     */

    private void drawGraphs(String sensor1, String sensor2, ImageView image1, ImageView image2){
        CardHandler ch = new CardHandler(this);
        DisplayManager dsm = new DisplayManager();
        dsm.connectLines(preferences.drawlines);
        if ((sensor1 != "") && (image1 != null)){
            ArrayList<SensorDataSet> data1 = new ArrayList<SensorDataSet>();
            data1 = ch.getSensorDataSetArrayByTimeInterval(sensor1, (long) GRAPH_TIMESPAN);
            if (data1.size()>0){
                SensorDataSet sds = data1.get(0);
                if (sds.getSensorType() == SensorDataSet.PARTICULATE_SENSOR)
                    dsm.drawParticulate(context, data1,image1);
                if (sds.getSensorType() == SensorDataSet.TEMPERATURE_SENSOR)
                    dsm.drawTemperature(context, data1,image1);
            }
        }
        if ((sensor2 != "") && (image2 != null)){
            ArrayList<SensorDataSet> data2 = new ArrayList<SensorDataSet>();
            data2 = ch.getSensorDataSetArray(sensor2);
            if (data2.size()>0){
                SensorDataSet sds = data2.get(0);
                if (sds.getSensorType() == SensorDataSet.PARTICULATE_SENSOR)
                    dsm.drawParticulate(context,data2,image2);
                if (sds.getSensorType() == SensorDataSet.TEMPERATURE_SENSOR)
                    dsm.drawTemperature(context,data2,image2);
            }
        }
    }

    /**
     * Convenience call for the above method that determines the view ids and
     * passes them over.
     */

    private void drawGraphs(){
        ScrollView mainviewcontainer = (ScrollView) findViewById(R.id.main_maincontainer_layout);
        graph1 = (ImageView) findViewById(R.id.main_graph1);
        graph2 = (ImageView) findViewById(R.id.main_graph2);
        mainviewcontainer.post(new Runnable() {
            @Override
            public void run() {
                drawGraphs(preferences.sensor1_number, preferences.sensor2_number, graph1, graph2);
            }
        });
    }

    /**
     * This class handles the data update for the local sensors from the API.
     */

    private class ApiReader extends FetchSensorDataFromAPI {
        public ApiReader(Context c){
            super(c);
        }

        @Override
        public void onPositiveResult(){
            releaseUpdateBlocker();
            updateDataDisplays();
            refreshWidgets(REPFRESH_LOCAL_WIDGETS);
        }

        @Override
        public void onNegativeResult(){
            releaseUpdateBlocker();
            Toast toast = Toast.makeText(context,getApplicationContext().getResources().getString(R.string.main_refresh_nonetwork), Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private Boolean isLocalSensorDataTooOld(SensorDataSet[] sensorDataSets){
        if (sensorDataSets == null)
            return true;
        if (sensorDataSets.length==0)
            return true;
        Calendar c = Calendar.getInstance();
        long time = 0;
        for (int i=0; i<sensorDataSets.length; i++){
            if (sensorDataSets[i].getTimestampLocalMillis()>time)
                time = sensorDataSets[i].getTimestampLocalMillis();
        }
        if (c.getTimeInMillis()-time<StaubWidget.WIDGET_CONN_RETRY_DELAY/2){
            return false;
        }
        return true;
    }

    /**
     * This call updates the local sensors with new data form the API.
     *
     * The API is only called if the local data is "too old".
     *
     * "too old" is defined by WIDGET_CONN_RETRY_DELAY/2.
     */

    private void getLocalSensorDataFromAPI(){
        if (updateCloseSensors_is_running){
            return;
        }
        try {
            readPreferences();
            CardHandler ch = new CardHandler(this);
            URL url1 = null;
            URL url2 = null;
            SensorDataSet sensor1_data = null;
            SensorDataSet sensor2_data = null;
            if (!preferences.sensor1_number.equals("")){
                url1 = ch.getSensorAPIURL(preferences.sensor1_number);
                sensor1_data = ch.getLatestDataSet(preferences.sensor1_number);
            }
            if (!preferences.sensor2_number.equals("")){
                url2 = ch.getSensorAPIURL(preferences.sensor2_number);
                sensor2_data = ch.getLatestDataSet(preferences.sensor2_number);
            }
            SensorDataSet[] sensorDataSets = {sensor1_data,sensor2_data};
            if (isLocalSensorDataTooOld(sensorDataSets)){
                setUpdateBlocker(0);
                ApiReader apireader = new ApiReader(this);
                apireader.execute(url1,url2);
            } else {
                setUpdateBlocker(0);
                displayLocalDataset();
                releaseUpdateBlocker();
            }
        } catch (Exception e){
            releaseUpdateBlocker();
        } finally {
            // nothing to do.
        }
    }

    /**
     * The three arrays hold the information for the mobile display.
     * close_sensors: the (raw) list of the next close sensors. This array
     * should not be used further than by the getCloseDataSet class to populate
     * the two arrays described below.
     *
     * The other two arrays have no sensors with empty data. They are used for
     * the display. The arraylist ids match the sensors.
     *
     * nextSensors_location: the location data of the close sensors
     * nextSensors_data: the data of the close sensors
     */

    private ArrayList<LocationDataSet> close_sensors = new ArrayList<LocationDataSet>();
    private ArrayList<LocationDataSet> nextSensors_location;
    private ArrayList<SensorDataSet> nextSensors_data;

    /**
     * Local stuff needed for the getCloseDataSet class.
     */

    private int datafillcount = 0;
    private int sensorlist_index = 0;

    /**
     * This is the number of close sensors to display. Please note: this CANNOT be
     * easily modified. Changing this number would make it necessary to adapt the
     * activity_main.xml file and the according arrays in this class.
     */

    private final static int SENSORCOUNT_TO_LIST = 6;


    private void updateDataTimeOfTable(){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView status = (TextView) findViewById(R.id.main_nextsensors_lastupdate_text);
                table_lastupdatetext = getResources().getString(R.string.main_location_lastupdate_time)+" "+getCurrentTimeString();
                if (status != null){
                    status.setText(table_lastupdatetext);
                    status.setVisibility(View.VISIBLE);
                    status.invalidate();
                }
            }
        });
    }

    private void display_nextSensors_TableHeading(){
        TextView label1 = (TextView) findViewById(R.id.main_nextsensors_value1_0);
        TextView label2 = (TextView) findViewById(R.id.main_nextsensors_value2_0);
        if (getSelectedVisualizationType()==VISUALIZE_TEMPERATURE){
            label1.setText(getResources().getString(R.string.main_temperature));
            label2.setText(getResources().getString(R.string.main_humidity));
        } else {
            label1.setText(getResources().getString(R.string.main_particles1));
            label2.setText(getResources().getString(R.string.main_particles2));
        }
    }

    /**
     * Fills the mobile data table textviews with values. This sub fills one row (range 1-6) with
     * the necessary data from the LocationDataSet l and the SensorDataSet s.
     */


    private void display_nextSensors_Row(final int row, final LocationDataSet l, final SensorDataSet s){
        if (row>=SENSORCOUNT_TO_LIST){
            return;
        }
        /*
         * The display routines are defined within a runnable to execute them on the UI thread. This
         * way, it is safe to access the views from an other thread.
         */
        Runnable r = new Runnable(){
            @Override
            public void run(){
                DisplayManager dm = new DisplayManager();
                String unit_distance = context.getResources().getString(R.string.main_distance_unit);
                String unit_string_value1;
                String unit_string_value2;
                TextView textView_sensor_id   = (TextView)  findViewById(locationtable_sensor_id[row]);
                ImageView imageView_direction = (ImageView) findViewById(locationtable_direction_id[row]);
                TextView textView_distance    = (TextView)  findViewById(locationtable_distance_id[row]);
                TextView textView_value1      = (TextView)  findViewById(locationtable_value1_id[row]);
                TextView textView_value2      = (TextView)  findViewById(locationtable_value2_id[row]);
                TableRow tableRow             = (TableRow)  findViewById(locationtable_row_id[row]);
                if (s!=null) {
                    if (s.getSensorType()==SensorDataSet.PARTICULATE_SENSOR){
                        unit_string_value1 = context.getResources().getString(R.string.main_particulate_unit);
                        unit_string_value2 = context.getResources().getString(R.string.main_particulate_unit);
                    } else {
                        unit_string_value1 = context.getResources().getString(R.string.main_temperature_unit);
                        unit_string_value2 = context.getResources().getString(R.string.main_humidity_unit);
                    }
                    if (textView_sensor_id != null){
                        textView_sensor_id.setText(String.valueOf(s.sensorId));
                    }
                    if (textView_value1 != null) {
                        textView_value1.setText(s.sensordataValue[0]+" "+unit_string_value1);
                    }
                    if (textView_value2 != null){
                        textView_value2.setText(s.sensordataValue[1]+" "+unit_string_value2);
                    }
                } else {
                    if (textView_sensor_id != null){
                        textView_sensor_id.setText("-");
                    }
                    if (textView_value1 != null) {
                        textView_value1.setText("-");
                    }
                    if (textView_value2 != null){
                        textView_value2.setText("-");
                    }
                }
                if (l!=null){
                    if (imageView_direction != null){
                        imageView_direction.setImageBitmap(dm.getArrowBitmap(context,l.bearing));
                    }
                    if (textView_distance != null) {
                        textView_distance.setText(String.valueOf(Math.round(l.distance))+" "+unit_distance);
                    }
                } else {
                    if (imageView_direction != null){
                        imageView_direction.setImageBitmap(null);
                    }
                    if (textView_distance != null) {
                        textView_distance.setText("-");
                    }
                }
                if (tableRow != null){
                    tableRow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (s!=null){
                                if (s.sensorId != null){
                                    Intent i = new Intent(context, SensorInfo.class);
                                    i.putExtra(DATA_SENSORNUMBER, s.sensorId);
                                    startActivity(i);
                                }
                            }
                        }
                    });
                }
            }
        };
        // execute the update of the rows on the UI thread.
        this.runOnUiThread(r);
    }

    /**
     * This class extends the FetchSensorDataFromAPI class.
     *
     * It reads the sensor data from a close sensor, checks if values are present,
     * fills the nextSensors_data and nextSensors_location arraylists and updates
     * the mobile data table row by row.
     *
     * New data is not only saved locally in the arrays but also added to the
     * local SQLite database with sensor data.
     *
     * This way, the data can later be accessed without re-querying the API.
     *
     * The "updateCloseSensors_is_running" variable is set to true as long as
     * an update from the API is running. This prevents certain tasks that may
     * lead to running conditions and/or inappropriate results.
     *
     * As long as "updateCloseSensors_is_running" is true, the app/user cannot:
     * - re-update the display with new data from the API
     * - switch between the fine particulate matter and the temperature/
     *   humidity display
     */

    private class getCloseDataSet extends FetchSensorDataFromAPI{

        public getCloseDataSet(Context c){
            super(c);
       }

        @Override
        public void onNegativeResult(){
            super.onNegativeResult();
            /*
             * when the update is finished, it is now possible to
             * allow a subsequent update by setting "updateCloseSensors_is_running" to false
             */
            releaseUpdateBlocker();
        }

        @Override
        public void onPositiveResult(){
            super.onPositiveResult();
            /*
             * when the update is finished, it is now possible to
             * allow a subsequent update by setting "updateCloseSensors_is_running" to false
             */
            releaseUpdateBlocker();
            /*
             * As the new data was read successfully from the API, we can now update
             * all data displays including:
             * - the map/chart,
             * - local display
             */
            updateDataDisplays();
            refreshWidgets(REPFRESH_CLOSE_WIDGETS);
            /*
             * We now update the time of the last update
             */
            updateDataTimeOfTable();
            /*
             * To get sure that nothing was mixed up, we finally check that the list does not
             * display fine particulate matter and temperature/humidity at one time.
             */
            checkForLocalListIntegrity();
        }

        @Override
        protected void onPostExecute(ArrayList<FullSensorDataSet> content) {
            if (content == null) {
                // No data was received => API could not be accessed successfully, cancel the update.
                onNegativeResult();
            } else {
                // Get an instance of the CardHandler
                CardHandler ch = new CardHandler(context);
                // Get the next close sensor from the list
                LocationDataSet l = close_sensors.get(sensorlist_index);
                /*
                 Read the data sequentially, as "content" may hold multiple sensors.
                 */
                    for (int i=0; i<content.size(); i++){
                        SensorDataSet s = content.get(i);
                        /*
                         * We use the data set only if it is not empty (means holds some values).
                         */
                       if ((!s.sensordataValue[0].equals("")) && (!s.sensordataValue[1].equals(""))){
                            // sensor has data
                            // add data to local database
                            ch.addSensorDataSetIfNew(s);

                            // getContentResolver().insert(LuftDatenContentProvider.URI,CardHandler.getInstance(context).getContentValuesFromSensorDataSet(s));

                            // add to array
                           if (verbose) {
                               Log.v("FSW: ","i: "+String.valueOf(i)+" Sensor:"+s.sensorId+"/"+l.number+" L1: "+s.sensordataValue[0]+" L2: "+s.sensordataValue[1]);
                           }
                           /* We add the sensor data to the local arrays, assuming the last sensor set
                            * is the newest.
                            */
                           if (i==content.size()-1){
                               try {
                                   nextSensors_data.add(s);
                                   nextSensors_location.add(l);
                               } catch (Exception e){
                                   // do nothing
                               }
                               // Display all the information in the row
                               display_nextSensors_Row(datafillcount,l,s);
                               datafillcount = datafillcount + 1;
                               updateProgressBar(datafillcount);
                           }
                    }
                }
                /*
                 *  If we do not have reached the end of the list (datafillcount<SENSORCOUNT_TO_LIST)
                 *  AND we have not considered all close sensors retrieved (sensorlist_index<close_sensors.size()-1),
                 *  we call getCloseDataSet recursively to get the next sensor row.
                 *  The variables mean:
                 *  datafillcount: rows filled with data
                 *  sensorlist_index: current position in the close_sensor arraylist.
                 */
                if ((datafillcount<SENSORCOUNT_TO_LIST) && (sensorlist_index<close_sensors.size()-1)) {
                    // get next sensor data
                    sensorlist_index = sensorlist_index + 1;
                    l = close_sensors.get(sensorlist_index);
                    getCloseDataSet newtask = new getCloseDataSet(context);
                    newtask.execute(ch.getSensorAPIURL(l.number));
                } else {
                    /*
                     *  The data was fetched and processed successfully. This is placed within the "else" clause
                     *  to ensure that "onPositiveResult()" is called only once by the last instance of
                     *  getCloseDataSet. Remember that this instance was called recursively and we have
                     *  SENSORCOUNT_TO_LIST instances of getCloseDataSet running, when this code is executed.
                     */
                    onPositiveResult();
                }
            }
        }
    }

    /**
     * The following routine handles the switch between the display of
     * fine particulate matter OR temperature/humidity in the mobile view.
     */

    private final static Boolean VISUALIZE_PARTICULATE = false;
    private final static Boolean VISUALIZE_TEMPERATURE = true;

    /**
     * Reads the value of the switch.
     *
     * @return is "true" or "false", meaning checked or not checked.
     */

    public Boolean getSelectedVisualizationType(){
        Switch s = (Switch) findViewById(R.id.main_nextsensors_dataswitch);
        if (s!=null){
            if (s.isChecked()){
                return true;
            }
        }
        return false;
    }

    /**
     * Updates the mobile data display by reading new data from the API.
     *
     * @param location a Location instance to use for the data update.
     */

    private void updateCloseSensorsDataFromAPI(Location location){
        if (updateCloseSensors_is_running){
            if (verbose){
                Log.v("FSW: ","Sorry, update is already running.");
            }
            return;
        }
        setViewIdArrays();
        if (verbose) {
            Log.v("FSW: ","----------------------------------------------------------");
            Log.v("FSW: ","UPDATING LIST FROM ***API DATA***");
            Log.v("FSW: ","----------------------------------------------------------");
            Log.v("FSW: ","Updating location!");
        }
        // block parallel updates and display the update status.
        setUpdateBlocker(6);
        SensorlocationHandler h = new SensorlocationHandler(this);
        int[] sensortypes = new int[1];
        if (getSelectedVisualizationType() == VISUALIZE_PARTICULATE){
            sensortypes[0] = SensorDataSet.PARTICULATE_SENSOR;
        } else {
            sensortypes[0] = SensorDataSet.TEMPERATURE_SENSOR;
        }
        close_sensors = h.getNextSensors(location,20,sensortypes);
        if (close_sensors != null){
            if (close_sensors.size()>0){
                if (verbose){
                    for (int i=0; i<close_sensors.size(); i++) {
                        LocationDataSet l = close_sensors.get(i);
                        Log.v("FSW (from ***API***):", "Nr. " + String.valueOf(i) + " => Sensor-id: " + String.valueOf(l.number) + " => Type: " + String.valueOf(l.type) + " => " + l.getDistance() + " m (" + l.bearing + ")");
                    }
                }
                // Update
                display_nextSensors_TableHeading();
                datafillcount = 0;
                sensorlist_index = 0;
                nextSensors_data = new ArrayList<>();
                nextSensors_location = new ArrayList<>();
                LocationDataSet l = close_sensors.get(0);
                CardHandler ch = new CardHandler(context);
                getCloseDataSet task = new getCloseDataSet(context);
                task.execute(ch.getSensorAPIURL(l.number));
            } else {
                // close_sensors.size() == 0;
                releaseUpdateBlocker();
            }
        } else {
            // if close_sensors == null
            releaseUpdateBlocker();
        }
    }

    /**
     * This call does pretty much the same like the getCloseDataSet class, but is far more
     * simple as it does not call the API but gets the data from the local SQLite
     * data base of the app.
     *
     * It is used by updateCloseSensorsFromPresentData(Location location).
     *
     * @param location_count_index indicating the row to process.
     */

    private void getCloseDataSet(int location_count_index){
        LocationDataSet l = close_sensors.get(location_count_index);
        CardHandler ch = new CardHandler(context);
        SensorDataSet s = ch.getLatestDataSet(l.number);
        if (s != null){
            if ((!s.sensordataValue[0].equals("")) && (!s.sensordataValue[1].equals(""))) {
                display_nextSensors_Row(datafillcount, l, s);
                nextSensors_location.add(l);
                nextSensors_data.add(s);
                datafillcount = datafillcount + 1;
                }
            }
        if ((datafillcount<SENSORCOUNT_TO_LIST) && (location_count_index+1<close_sensors.size())){
            getCloseDataSet(location_count_index + 1);
        } else {
            releaseUpdateBlocker();
            updateCloseSensorsMap();
        }
    }

    /**
     * This call updates the close sensor display using locally available data from
     * the SQLite data base of the app.
     *
     * It exits immediately should a data update from the API be running at the same time.
     * The update from the API is more accurate and has priority.
     *
     * @param location
     */

    private void updateCloseSensorsFromPresentData(Location location){
        /*
         * Abort the local data restoration if a data collection from the API is
         * currently running. The latter will be more accurate anyway. Continuing
         * would generate a "running condition".
         */
        if (updateCloseSensors_is_running){
            return;
        }
        if (verbose){
            Log.v("FSW: ","----------------------------------------------------------");
            Log.v("FSW: ","UPDATING LIST FROM PRESENT, LOCAL DATA");
            Log.v("FSW: ","----------------------------------------------------------");
        }
        setViewIdArrays();
        display_nextSensors_TableHeading();
        SensorlocationHandler h = new SensorlocationHandler(this);
        int[] sensortypes = new int[1];
        if (dataselectorposition == FeinstaubPreferences.DATASWITCHPOSITION_PARTICULATEMATTER){
            sensortypes[0] = SensorDataSet.PARTICULATE_SENSOR;
        } else {
            sensortypes[0] = SensorDataSet.TEMPERATURE_SENSOR;
        }
        close_sensors = h.getNextSensors(location,20,sensortypes);
        if (close_sensors != null){
            if (close_sensors.size()>0){
                for (int i=0; i<close_sensors.size(); i++) {
                    LocationDataSet l = close_sensors.get(i);
                }
                datafillcount = 0;
                sensorlist_index = 0;
                nextSensors_data = new ArrayList<>();
                nextSensors_location = new ArrayList<>();
                setUpdateBlocker(0);
                getCloseDataSet(0);
                checkForLocalListIntegrity();
            }
        }

    }

    /**
     * This call updates the close sensor display using data available in local variables.
     * This is mainly used to restore the display after screen rotation.
     *
     * The result should be pretty much the same like by calling
     * updateCloseSensorsFromPresentData(Location location), but this version is much
     * faster in terms of performance and does not "freeze" the display too long.
     */

    private void updateCloseSensorsFromLocalVariables(){
        if (updateCloseSensors_is_running){
            return;
        }
        setViewIdArrays();
        try {
            display_nextSensors_TableHeading();
            if ((nextSensors_data != null) && (nextSensors_location != null)) {
                for (int i = 0; i < SENSORCOUNT_TO_LIST; i++) {
                    if ((i < nextSensors_data.size()) && (i < nextSensors_location.size())) {
                        SensorDataSet s = nextSensors_data.get(i);
                        LocationDataSet l = nextSensors_location.get(i);
                        display_nextSensors_Row(i, l, s);
                    }
                    checkForLocalListIntegrity();
                }
            }
            TextView v = (TextView) findViewById(R.id.main_nextsensors_lastupdate_text);
            if ((table_lastupdatetext != null) && (v != null)) {
                v.setText(table_lastupdatetext);
            }
            TextView x = (TextView) findViewById(R.id.main_nextsensors_devicelocation_text);
            if ((table_lastlocationtime != null) && (x != null)) {
                x.setText(table_lastlocationtime);
            }
        } catch (Exception e){
            // do nothing
        }
    }

    /**
     * Checks if the arrays
     * - nextSensors_data
     * - nextSensors_location
     * hold data for the same type of sensor.
     *
     * If not, the table content is reset to zero.
     *
     * @return
     */

    private Boolean checkForLocalListIntegrity() {
        Boolean integrity = true;
        if (nextSensors_data == null)
            return true;
        if (nextSensors_data.size()==0)
            return true;
        SensorDataSet s0 = nextSensors_data.get(0);
        for (int i=0; i<nextSensors_data.size(); i++){
            SensorDataSet s = nextSensors_data.get(i);
            if (s.getSensorType() != s0.getSensorType()){
                clearCloseSensorsLocalVariables();
                return false;
            }
        }
        return true;
    }

    /**
     * Resets the variables of the mobile display.
     */

    private void clearCloseSensorsLocalVariables(){
        nextSensors_data = null;
        nextSensors_location = null;
        table_lastlocationtime = null;
        table_lastupdatetext = null;
        for (int i=0; i<SENSORCOUNT_TO_LIST; i++){
            display_nextSensors_Row(i,null,null);
        }
        ImageView i = (ImageView) findViewById(R.id.main_closesensors_graph);
        if (i!=null){
            i.setImageBitmap(null);
        }
        releaseUpdateBlocker();
    }

    /**
     * Displays (or hides) the reference display, following what has been set in the preferences.
     */

    public void checkForReferenceDisplay(){
        TextView t = (TextView) findViewById(R.id.main_reference_text);
        if (t != null){
            Boolean display_reference = preferences.readPreference(preferences.PREF_DISPLAY_REFERENCE,preferences.PREF_DISPLAY_REFERENCE_DEFAULT);
            if (display_reference) {
                t.setVisibility(View.VISIBLE);
                t.invalidate();
            } else {
                t.setVisibility(View.INVISIBLE);
                t.invalidate();
            }
        }
    }

    /**
     * Registers to location updates. This is only of importance if the location changes and the
     * app is in the foreground.
     */

    private void registerToLocationUpdates(){
        if (lm == null) {
            loc_listener = new FeinstaubLocationListener(this){
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null){
                        updateCloseSensors(location, true, true);
                    }
                }
                @Override
                public void onProviderEnabled(String s) {
                    displayLocationServiceStatus();
                    requestSingleLocationUpdate();
                }
                @Override
                public void onProviderDisabled(String s) {
                    displayLocationServiceStatus();
                }
            };
            loc_listener.setLocationContext(context);
            loc_listener.setLocationVariable(localLastKnownLocation);
            lm = new FeinstaubLocationManager(this);
            if (hasLocationPermission(false)) {
                try {
                    lm.requestLocationUpdates(loc_listener,preferences);
                } catch (Exception e){
                    // nothing to do; no suitable permissions granted to get the location.
                }
            } else {
                /*
                 * Set 1st sensor as location if this is the desired behavior.
                 */
                if (preferences == null)
                    readPreferences();
                if (preferences.use_local_sensor_location_as_fallback){
                    if (localLastKnownLocation == null){
                        localLastKnownLocation = lm.getLastKnownLocation(preferences);
                    }
                }
            }
        }
    }

    /**
     * This is a convenience method that checks if the variable last_known_location has a valid
     * location.
     *
     * If not, it tries to get a "last known location" from the system. It tries to get this
     * from the GPS_PROVIDER first. If such a location is not known, it tries to get the
     * location from the PASSIVE_PROVIDER. This is likely to be less accurate.
     *
     * @param c Context
     * @return last known location
     */

    private Location getLastKnownLocationIfNotKnown(Context c){
        if (lm == null){
            if (verbose)
                Log.v("MAIN","LocationManager is null.");
            registerToLocationUpdates();
        }
        if (lm != null){
            if (localLastKnownLocation == null) {
                    localLastKnownLocation = lm.getLastKnownLocation(preferences);
            }
        }
        return localLastKnownLocation;
    }


    /**
     * Requests a single location update. This is triggered manually.
     */

    public void requestSingleLocationUpdate(){
        FeinstaubLocationListener feinstaubLocationListener = new FeinstaubLocationListener(this){
            @Override
            public void onLocationChanged(Location location) {
                if (location!=null){
                    updateCloseSensors(location,true,true);
                }
            }
        };
        if (lm == null){
            lm = new FeinstaubLocationManager(context);
        }
        if (lm != null){
            try {
                lm.requestSingleUpdate(feinstaubLocationListener,null,preferences);
            } catch (Exception e){
                /*
                 * Location update failed. We do nothing here.
                 */
            }
        }
    }

    /**
     * Reads the last visible selector (mobile or local) from the preferences. Returns the value.
     *
     * @return
     */

    private int getCardSelectorValue(){
        return preferences.readPreference(FeinstaubPreferences.PREF_CARDSELECTORVALUE,FeinstaubPreferences.PREF_CARDSELECTORVALUE_DEFAULT);
    }

    /**
     * Stores the selector i in the preferences.
     *
     * @param i the visible selector to store.
     */

    private void storeCardSelectorValue(int i){
        preferences.commitPreference(FeinstaubPreferences.PREF_CARDSELECTORVALUE,i);
    }

    /**
     * Reads the last visible data selector position (fine particulate matter or temperature) from the
     * preferences.
     * Returns the value.
     *
     * @return
     */

    private Boolean getDataSelectorPosition(){
        return preferences.readPreference(FeinstaubPreferences.PREF_DATASWITCHPOSITION,FeinstaubPreferences.PREF_DATASWITCHPOSITION_DEFAULT);
    }

    /**
     * Stores the last visible data selector position.
     */

    private void storeDataSelectorPosition(Boolean position){
        preferences.commitPreference(FeinstaubPreferences.PREF_DATASWITCHPOSITION,position);
    }

    /**
     * Reads the preferences.
     */

    private void readPreferences() {
        preferences = new FeinstaubPreferences(this);
    }

    /**
     * Displays the intro dialog.
     */

    public void showIntroDialog(){
        introDialog = new Dialog(this);
        introDialog.getWindow().setBackgroundDrawableResource(R.mipmap.datasheet_background);
        introDialog.setContentView(R.layout.aboutdialog);
        introDialog.setTitle(getResources().getString(R.string.app_name));
        introDialog.setCancelable(true);
        String versionname = "-";
        String versioncode = "-";
        PackageManager pm = this.getPackageManager();
        try {
            versionname = this.getPackageManager().getPackageInfo(this.getPackageName(),0).versionName;
            versioncode = String.valueOf(this.getPackageManager().getPackageInfo(this.getPackageName(),0).versionCode);
        } catch (Exception e){
            // do nothing
        }
        String versionstring = versionname + " (build "+versioncode+")";
        TextView heading = (TextView) introDialog.findViewById(R.id.text_intro_headtext);
        heading.setText(getResources().getString(R.string.intro_headtext_text)+ System.getProperty("line.separator")+"version "+versionstring);
        ImageView iv = (ImageView) introDialog.findViewById(R.id.intro_headimage);
        iv.setImageResource(R.mipmap.ic_launcher);
        Button contbutton = (Button) introDialog.findViewById(R.id.intro_button);
        contbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                introDialog.dismiss();
            }
        });
        introDialog.show();
    }

    /**
     * Returns the bitmap to share, which is derived from the visible views.
     *
     * @param data_source this should be MainActivity.CARDSELECTOR_LOCAL or MainActivity.CARDSELECTOR_MOBILE
     * @return
     */

    public Bitmap getShareDataBitmap(int data_source){
        LinearLayout sourceLinearLayout = null;
        Bitmap resultBitmap = null;
        if (data_source==MainActivity.CARDSELECTOR_LOCAL) {
            sourceLinearLayout = (LinearLayout) findViewById(R.id.main_local_container);
        }
        if (data_source==MainActivity.CARDSELECTOR_MOBILE) {
            sourceLinearLayout = (LinearLayout) findViewById(R.id.main_nextsensors_displaycontainer);
        }
        if (sourceLinearLayout!=null){
            Bitmap layoutBitmap = Bitmap.createBitmap(sourceLinearLayout.getWidth(),sourceLinearLayout.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(layoutBitmap);
            sourceLinearLayout.draw(c);
            DisplayManager displayManager = new DisplayManager(this);
            resultBitmap = displayManager.attachReferenceLayerToBitmap(layoutBitmap);
        }
        return resultBitmap;
    }

    /**
     * Returns a filename skeleton: Feinstaub-yyyyDDMM-HHmmss.jpg
     */

    private String getFileNameSkeleton(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss",Locale.getDefault());
        return simpleDateFormat.format(c.getTime())+".jpg";
    }

    /**
     * Returns a filename skeleton: Feinstaub-yyyyDDMM-HHmmss-i.jpg with a number attached (i)
     * @param i is file number (0,1,2 ... etc).
     */

    private String getNumberedFileNameSkeleton(int i){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss",Locale.getDefault());
        return simpleDateFormat.format(c.getTime())+"-"+String.valueOf(i)+".jpg";
    }

    /**
     * Determines an unused file name.
     *
     * @param path Target path where to save.
     * @return
     */

    private String getUnusedFileName(File path){
        // first, try to get a filename without attached number
        String r = getFileNameSkeleton();
        File checkfile = new File(path,r);
        if (!checkfile.exists())
            return r;
        // if the above fails, get a filename with an attached number
        int i = 0;
        do {
            i = i + 1;
            r = getNumberedFileNameSkeleton(i);
            checkfile = new File(path,r);
        } while (checkfile.exists());
        return r;
    }

    /**
     * Checks if the app has permissions to read/write to internal storage. Requests permission if necessary.
     *
     * @return
     */

    private boolean hasReadWriteStorageRuntimePermission(int permissioncallback_id){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            // handle runtime permissions only if android is >= Marshmellow api 23
            if (this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},permissioncallback_id);
                // at this moment, we request the permission and leave with a negative result. The user needs to try saving again after permission was granted.
                return false;
            }
            else
            {
                // permission is granted, ok
                return true;
            }
        }
        else
        {
            // before api 23, permissions are always granted, so everything is ok
            return true;
        }
    }

    /**
     * Makes the image known to the media scanner.
     *
     * This is necessary for the saved image to become visible in the gallery after
     * saving was completed.
     *
     * @param uri
     */

    private void advertiseImageToMediaScanner(Uri uri){
        if (uri != null){
            String[] path = new String[1];
            path[0] = uri.getPath();
            String[] mime = new String[1];
            mime[0] = MimeTypeMap.getSingleton().getMimeTypeFromExtension(".jpg");
            MediaScannerConnection.scanFile(context, path, mime, new MediaScannerConnection.OnScanCompletedListener() {
                @Override
                public void onScanCompleted(String s, Uri uri) {
                    if (verbose)
                        Log.v("FSB","Image "+uri.getPath()+" successfully scanned by media scanner.");
                }
            });
        }
    }

    private Uri saveBitmapToStorage(Bitmap bitmap, int callback, Boolean verbose_saveresult){
        if (hasReadWriteStorageRuntimePermission(callback)){
            File path = new File(String.valueOf(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + File.separatorChar + getResources().getString(R.string.share_foldename)));
            path.mkdirs();
            String filename = getUnusedFileName(path);
            try {
                File file = new File(path,filename);
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG,80,fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();
                // make image visible in the gallery
                advertiseImageToMediaScanner(Uri.fromFile(file));
                if (verbose_saveresult){
                    Toast.makeText(this,getResources().getString(R.string.save_success),Toast.LENGTH_SHORT).show();
                }
                return Uri.fromFile(file);
            } catch (Exception e){
                if (verbose_saveresult){
                    Toast.makeText(this,getResources().getString(R.string.save_failed),Toast.LENGTH_LONG).show();
                }
                return null;
            }
        }
        // no writing/reading from storage permissions
        return null;
    }

    private class FileMediaScanner implements MediaScannerConnection.MediaScannerConnectionClient{

        private File file;
        private MediaScannerConnection mediaScannerConnection;
        private Boolean share;

        public FileMediaScanner(Context c, File f, Boolean s){
            share = s;
            file = f;
            mediaScannerConnection = new MediaScannerConnection(context,this);
            mediaScannerConnection.connect();
        }

        @Override
        public void onMediaScannerConnected() {
            mediaScannerConnection.scanFile(file.getAbsolutePath(),null);
        }

        @Override
        public void onScanCompleted(String s, Uri uri) {
            mediaScannerConnection.disconnect();
            if (share) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_SEND);
                i.setDataAndType(uri, "image/jpeg");
                i.putExtra(Intent.EXTRA_STREAM, uri);
                i.setType("image/jpeg");
                startActivity(Intent.createChooser(i, getResources().getString(R.string.share_text)));
            }
        }
    }

    private void shareVisibleData(int callback){
        // here, we need to ask for permissions. If the read/write storage permission is not present,
        // there will be a callback once the permission is granted. At this point, we should avoid giving
        // a "saving failed" message, because the callback might still result in a successful saving action.
        if (hasReadWriteStorageRuntimePermission(callback)){
            Uri resource = saveBitmapToStorage(getShareDataBitmap(visible_cardselector),callback,false);
            if (resource != null){
                File file = new File(resource.getPath());
                new FileMediaScanner(getApplicationContext(),file,true);
            } else {
                // sharing failed, no uri
                Toast.makeText(this,getResources().getString(R.string.share_failed),Toast.LENGTH_LONG).show();
            }
        }
    }

    private final int ACTION_SAVE = 1;
    private final int ACTION_SHARE = 2;

    private void displaySaveAndShareAlertDialog(final int action){
        View sharehintView = View.inflate(this,R.layout.sharehint,null);
        CheckBox do_not_show_again = (CheckBox) sharehintView.findViewById(R.id.sharehint_checkbutton);
        do_not_show_again.setChecked(false);
        do_not_show_again.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                         @Override
                                                         public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                                             preferences.do_not_show_sharehint_again = b;
                                                             preferences.applyPreference(FeinstaubPreferences.PREF_DO_NOT_SHOW_SHAREHINT_AGAIN,preferences.do_not_show_sharehint_again);
                                                         }
                                                     });
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, 0);
        alertDialogBuilder.setTitle(getResources().getString(R.string.sharehint_title));
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertDialogBuilder.setIcon(getResources().getDrawable(R.mipmap.ic_share_white_24dp,this.getTheme()));
        }
        alertDialogBuilder.setView(sharehintView);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(getResources().getString(R.string.sharehint_button_continue_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (action==ACTION_SAVE){
                    saveBitmapToStorage(getShareDataBitmap(visible_cardselector), PERMISSION_CALLBACK_STORAGE_SAVE,true);
                }
                if (action==ACTION_SHARE){
                    shareVisibleData(PERMISSION_CALLBACK_STORAGE_SHARE);
                }
            }
        });
        alertDialogBuilder.setNegativeButton(getResources().getString(R.string.sharehint_button_cancel_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        shareHint = alertDialogBuilder.show();
    }

    /**
     * Save or share visible data.
     *
     * @param action should be ACTION_SAVE or ACTION_SHARE.
     */

    private void saveOrShareCurrentVisibleData(int action){
        // this blocks saving/sharing while the views get updated with new data.
        if (!updateCloseSensors_is_running){
            if (preferences.do_not_show_sharehint_again){
                if (action==ACTION_SAVE){
                    saveBitmapToStorage(getShareDataBitmap(visible_cardselector), PERMISSION_CALLBACK_STORAGE_SAVE,true);
                }
                if (action==ACTION_SHARE){
                    shareVisibleData(PERMISSION_CALLBACK_STORAGE_SHARE);
                }
            } else {
                displaySaveAndShareAlertDialog(action);
            }
        }
    }

}
