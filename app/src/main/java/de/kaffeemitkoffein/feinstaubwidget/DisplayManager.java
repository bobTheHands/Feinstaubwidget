/**

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import android.content.Context;
import android.graphics.*;
import android.location.Location;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RemoteViews;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;


/**
 * This class draws the charts for the widgets and the main activity.
 *
 */

public class DisplayManager {

    private final String ID = "Luft ";
    private final int HOURS2_IN_MILLIS = 2*1000*60*60;
    Boolean paint_lines = true;

    public static final int WIDGET_TEXTSIZE_NORMAL = 11;
    public static final int WIDGET_TEXTSIZE_SMALL = 9;

    public static final int WIDGET_COLOR_THEME_LIGHT = 0;
    public static final int WIDGET_COLOR_THEME_DARK = 1;

    public static final int WIDGET_DEFAULT_OPACITY = 50;

    public static final float LARGE_ZOOM = (float) 1.5;

    int widget_colorTheme = WIDGET_COLOR_THEME_LIGHT;
    int widget_textcolor = Color.WHITE;
    int widget_textsize_normal = WIDGET_TEXTSIZE_NORMAL;
    int widget_textsize_small = WIDGET_TEXTSIZE_SMALL;
    Boolean display_units = true;
    Boolean label_x_axis = true;

    /**
     * Variables & constants defining the reference. This is hard-coded.
     */

    private String sourcetext = "data © Luftdaten.Info & contributors";
    private final static float SOURCEBORDER = 3;
    public static final int REFERENCE_DEFAULT_SIZE = 18;

    public DisplayManager(Context c){
        FeinstaubPreferences preferences = new FeinstaubPreferences(c);
        this.setTheme(preferences.widget_theme);
    }

    public DisplayManager(int theme){
        this.setTheme(theme);
    }

    public DisplayManager(){

    }

    /**
     * Convenience call for setting "paint_lines" to true. If set, lines will be drawn instead
     * of dots. The default is to draw dots, as this is scientifically the correct way to draw
     * the chart. However, for some users lines may look fancier.
     * @param b draw lines
     */

    public void connectLines(Boolean b){
        paint_lines = b;
    }

    /**
     * Convenience call to display or hide units in graphs. Default is to display units.
     * This only affects the graphs, not the text in the widgets.
     * @param b
     */

    public void setUnitsDisplay(Boolean b){
        display_units = b;
    }

    private int getBackgroundInt(int alpha){
        String hex_string = Integer.toHexString(Math.round((float)alpha * (float)2.55));
        if (hex_string.length()<2)
        {
            hex_string = "0" + hex_string;
        }
        if (getTheme() == WIDGET_COLOR_THEME_LIGHT)
        {
            hex_string = hex_string+"101010";
        }
        if (getTheme() == WIDGET_COLOR_THEME_DARK)
        {
            hex_string = hex_string+"ffffff";
        }
        return Color.parseColor("#"+hex_string);
    }

    /**
     * Calculates the approx. widget size based on the cell size. Works both for width and height.
     *
     * @param c Context
     * @param cells Widget size in cells
     * @return pixel count
     */

    public float calculateWidgetSizeInPixelsFromCells(Context c, float cells){
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,cells * 70 - 30,c.getResources().getDisplayMetrics());
    }

    public float calculateWidgetSizeInPixelsFromDP(Context c, float dp){
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,dp,c.getResources().getDisplayMetrics());
    }

    /**
     * Convenience call to set the labeling of the x-axis. Default is true.
     *
     * @param b
     */
    public void setLabelXAxis(Boolean b){
        label_x_axis = b;
    }

    /**
     * Calculates the approx. font height in pixels.
     *
     * @param c
     * @param textsize
     * @return
     */

    public float calculateWidgetTextHightInPixels(Context c, int textsize){
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,textsize,c.getResources().getDisplayMetrics());
    }

    /**
     * Sets the widget color theme.
     *
     * @param t theme
     */

    public void setTheme(int t){
        widget_colorTheme = t;
        if (widget_colorTheme == WIDGET_COLOR_THEME_DARK){
            widget_textcolor = Color.DKGRAY;
        } else {
            widget_textcolor = Color.WHITE;
        }
    }

    /**
     * Gets the current widget color theme.
     *
     * @return t theme
     */

    public int getTheme(){
        return widget_colorTheme;
    }

    /**
     * Gets the int color value from the theme and the opacity.
     * Used to set the widget background.
     *
     * @param alpha
     * @return
     */


    /**
     * Convenience method to display the data in the main app.
     *
     * @param context
     * @param sensordata
     * @param sensor
     * @param time1
     * @param time2
     * @param datalabel1
     * @param datalabel2
     * @param data1
     * @param data2
     *
     */

    public void displayCurrentDataset(Context context, SensorDataSet sensordata, TextView sensor, TextView time1, TextView time2, TextView datalabel1, TextView datalabel2, TextView data1, TextView data2, TextView location_c, TextView location_long, TextView location_lat, TextView location_alt){
        String label1="-";
        String label2="-";
        String unit1="-";
        String unit2="-";
        if (sensordata.getSensorType()==SensorDataSet.PARTICULATE_SENSOR){
            label1 = context.getApplicationContext().getResources().getString(R.string.main_particles1);
            label2 = context.getApplicationContext().getResources().getString(R.string.main_particles2);
            unit1 = context.getApplicationContext().getResources().getString(R.string.main_particulate_unit);
            unit2 = context.getApplicationContext().getResources().getString(R.string.main_particulate_unit);
        }
        if (sensordata.getSensorType()==SensorDataSet.TEMPERATURE_SENSOR){
            label1 = context.getApplicationContext().getResources().getString(R.string.main_temperature);
            label2 = context.getApplicationContext().getResources().getString(R.string.main_humidity);
            unit1 = context.getApplicationContext().getResources().getString(R.string.main_temperature_unit);
            unit2 = context.getApplicationContext().getResources().getString(R.string.main_humidity_unit);
        }
        if (sensor != null)
            sensor.setText(sensordata.sensorId);
        if (time1 != null){
            time1.setText(context.getResources().getString(R.string.main_sensor_timestamp_label)+" ("+sensordata.getDatestampLocalTime()+"):");
        }
        if (time2 != null)
            time2.setText(sensordata.getTimestampLocalTime());
        if (datalabel1 != null)
            datalabel1.setText(label1);
        if (datalabel2 != null)
            datalabel2.setText(label2);
        if (data1 != null)
            data1.setText(sensordata.sensordataValue[0]+" "+unit1);
        if (data2 != null)
            data2.setText(sensordata.sensordataValue[1]+" "+unit2);
        if (location_c != null)
            location_c.setText(sensordata.locationCountry);
        if (location_long != null)
            location_long.setText(sensordata.locationLongitude);
        if (location_lat != null)
            location_lat.setText(sensordata.locationLatitude);
        if (location_alt != null)
            location_alt.setText(sensordata.locationAltitude);
    }

    /**
     * Convenience method to display the data in the big widget.
     *
     * @param wdm
     * @param sensordata
     * @param rV
     * @param line1
     * @param line2
     * @param line3
     * @param graph
     * @param onlytwolines
     */

    public RemoteViews displayCurrentDataSetBig(WidgetDimensionManager wdm, SensorDataSet sensordata, RemoteViews rV, int line1, int line2, int line3, int graph, Boolean onlytwolines, Boolean display_reference, int opacity){
        float widget_textsize = widget_textsize_normal;
        if (onlytwolines)
            widget_textsize = widget_textsize_small;
        String unit1 = null;
        String unit2 = null;
        String valuelabel1 = null;
        String valuelabel2 = null;
        String timetext = null;
        String line1text = "";
        String line2text = "";
        String line3text = "";
        Bitmap bitmap = null;
        RemoteViews remoteViews = rV;
        remoteViews.setInt(R.id.widget_maincontainer_big_relative,"setBackgroundColor",getBackgroundInt(opacity));
        if (sensordata == null){
            if (line1 !=0){
                remoteViews.setViewVisibility(line1, View.VISIBLE);
                remoteViews.setTextColor(line1,Color.WHITE);
                remoteViews.setTextViewTextSize(line1,TypedValue.COMPLEX_UNIT_SP,widget_textsize);
                remoteViews.setTextViewText(line1,wdm.context.getResources().getString(R.string.widget_nodata_message));
            }
            if (line2 !=0)
                remoteViews.setViewVisibility(line2, View.GONE);
            if (line3 !=0)
                remoteViews.setViewVisibility(line3, View.GONE);
            if (graph != 0)
                remoteViews.setViewVisibility(graph, View.GONE);
            return remoteViews;
        }
        float textheight = wdm.getFontHeightInPixels(widget_textsize)*3;
        if (onlytwolines){
            textheight = wdm.getFontHeightInPixels(widget_textsize)*2;
        }
        float chart_width  = (float) wdm.getWidgetWidth();
        float chart_height = (float) (wdm.getWidgetHeight()*1.45-textheight);
        ArrayList<SensorDataSet> sensordataarraylist = new ArrayList<SensorDataSet>();
        CardHandler ch = new CardHandler(wdm.context);
        sensordataarraylist = ch.getSensorDataSetArray(sensordata.sensorId);
        if ( sensordata.getSensorType() == SensorDataSet.PARTICULATE_SENSOR ){
            valuelabel1 = wdm.context.getResources().getString(R.string.widget_particles1);
            valuelabel2 = wdm.context.getResources().getString(R.string.widget_particles2);
            unit1 = wdm.context.getResources().getString(R.string.main_particulate_unit);
            unit2 = wdm.context.getResources().getString(R.string.main_particulate_unit);
            bitmap = getParticulateChart(wdm.context,sensordataarraylist,chart_width,chart_height);
        }
        if ( sensordata.getSensorType() == SensorDataSet.TEMPERATURE_SENSOR ) {
            valuelabel1 = wdm.context.getResources().getString(R.string.widget_temperature);
            valuelabel2 = wdm.context.getResources().getString(R.string.widget_humidity);
            unit1 = wdm.context.getResources().getString(R.string.main_temperature_unit);
            unit2 = wdm.context.getResources().getString(R.string.main_humidity_unit);
            bitmap = getTemperatureChart(wdm.context,sensordataarraylist,chart_width,chart_height);
        }
        timetext = sensordata.getTimestampLocalTime();
        if (line1 != 0){
            line1text = wdm.context.getResources().getString(R.string.widget_sensornumber) + " " + sensordata.sensorId;
            if (timetext != null)
                line1text = line1text + " " + wdm.context.getResources().getString(R.string.widget_time_symbol) + timetext;
            remoteViews.setViewVisibility(line1, View.VISIBLE);
            remoteViews.setTextViewText(line1,line1text);
        }
        if ((valuelabel1 != null) && (unit1 != null))
            line2text = valuelabel1 + " " + sensordata.sensordataValue[0] + " " + unit1;
        if ((valuelabel2 != null) && (unit2 != null))
            line3text = valuelabel2 + " " + sensordata.sensordataValue[1] + " " + unit2;
        String longeststring = "";
        Paint textpaint = new Paint();
        textpaint.setTextSize(wdm.getFontHeightInPixels(widget_textsize));
        if (textpaint.measureText(line1text) > textpaint.measureText(longeststring)){
            longeststring = line1text;
        }
        if (textpaint.measureText(line2text) > textpaint.measureText(longeststring)){
            longeststring = line2text;
        }
        if (textpaint.measureText(line3text) > textpaint.measureText(longeststring)){
            longeststring = line3text;
        }
        while (textpaint.measureText(longeststring) > wdm.getWidgetWidth()*0.4){
            widget_textsize = widget_textsize - 1;
            textpaint.setTextSize(wdm.getFontHeightInPixels(widget_textsize));
        }
        remoteViews.setTextColor(line1,widget_textcolor);
        remoteViews.setTextColor(line2,widget_textcolor);
        remoteViews.setTextColor(line3,widget_textcolor);
        if (onlytwolines){
            line2text = line2text + " " + line3text;
            remoteViews.setViewVisibility(line3, View.GONE);
            remoteViews.setTextViewTextSize(line1, TypedValue.COMPLEX_UNIT_SP,widget_textsize);
            remoteViews.setTextViewTextSize(line2, TypedValue.COMPLEX_UNIT_SP,widget_textsize);
            line3 = 0;
        } else {
            remoteViews.setTextViewTextSize(line1, TypedValue.COMPLEX_UNIT_SP,widget_textsize);
            remoteViews.setTextViewTextSize(line2, TypedValue.COMPLEX_UNIT_SP,widget_textsize);
            remoteViews.setTextViewTextSize(line3, TypedValue.COMPLEX_UNIT_SP,widget_textsize);
        }
        if (line2 != 0){
            remoteViews.setViewVisibility(line2, View.VISIBLE);
            remoteViews.setTextViewText(line2,line2text);
        }
        if (line3 != 0){
            remoteViews.setViewVisibility(line3, View.VISIBLE);
            remoteViews.setTextViewText(line3,line3text);
        }
        if (graph != 0){
            remoteViews.setViewVisibility(graph, View.VISIBLE);
            remoteViews.setImageViewBitmap(graph,bitmap);
        }
        if (display_reference) {
            remoteViews.setViewVisibility(R.id.widget_big_reference_text, View.VISIBLE);
            remoteViews.setTextColor(R.id.widget_big_reference_text,widget_textcolor);
        } else {
            remoteViews.setViewVisibility(R.id.widget_big_reference_text, View.GONE);
        }
        return remoteViews;
    }

    /**
     * Fills the fields of the small widget with data.
     *
     * @param wdm The app context.
     * @param remoteViews The RemoteViews instance of the widget
     * @param time The ID of the time view
     * @param sensor1_data1 The ID of the sensor #1 data #1 view. Can be 0 to get omitted.
     * @param sensor1_data2 The ID of the sensor #1 data #2 view. Can be 0 to get omitted.
     * @param sensor2_data1 The ID of the sensor #2 data #1 view. Can be 0 to get omitted.
     * @param sensor2_data2 The ID of the sensor #2 data #2 view. Can be 0 to get omitted.
     * @param data1 SensorDataSet of sensor #1. Can be null. If null, the views will be made invisible.
     * @param data2 SensorDataSet of sensor #2. Can be null. If null, the views will be made invisible.
     *
     *
     */

    public void displayCurrentDataSet(WidgetDimensionManager wdm, RemoteViews remoteViews, int time, int sensor1_data1, int sensor1_data2, int sensor2_data1, int sensor2_data2, SensorDataSet data1, SensorDataSet data2, Boolean display_reference, int opacity) {
        String time1 = null;
        String time2 = null;
        String line1 = null;
        String line2 = null;
        String line3 = null;
        String line4 = null;
        if ((data1 == null) && (data2 == null)){
            time1 = "--:--";
            time2 = "--:--";
            line1 = wdm.context.getResources().getString(R.string.widget_nodata_short1);
            line2 = wdm.context.getResources().getString(R.string.widget_nodata_short2);
            line3 = wdm.context.getResources().getString(R.string.widget_nodata_short3);
            line4 = wdm.context.getResources().getString(R.string.widget_nodata_short4);
        }
        remoteViews.setInt(R.id.widget_maincontainer,"setBackgroundColor",getBackgroundInt(opacity));
        if (data1 != null) {
            time1 = data1.getTimestampLocalTime();
            if (data1.getSensorType() == SensorDataSet.PARTICULATE_SENSOR) {
                line1 = wdm.context.getResources().getString(R.string.widget_particles1) + " " + data1.sensordataValue[0] + " " + wdm.context.getResources().getString(R.string.main_particulate_unit);
                line2 = wdm.context.getResources().getString(R.string.widget_particles2) + " " + data1.sensordataValue[1] + " " + wdm.context.getResources().getString(R.string.main_particulate_unit);
            }
            if (data1.getSensorType() == SensorDataSet.TEMPERATURE_SENSOR) {
                line1 = wdm.context.getResources().getString(R.string.widget_temperature) + " " + data1.sensordataValue[0] + " " + wdm.context.getResources().getString(R.string.main_temperature_unit);
                line2 = wdm.context.getResources().getString(R.string.widget_humidity) + " " + data1.sensordataValue[1] + " " + wdm.context.getResources().getString(R.string.main_humidity_unit);
            }
        }
        if (data2 != null) {
            time2 = data2.getTimestampLocalTime();
            if (data2.getSensorType() == SensorDataSet.PARTICULATE_SENSOR) {
                line3 = wdm.context.getResources().getString(R.string.widget_particles1) + " " + data2.sensordataValue[0] + " " + wdm.context.getResources().getString(R.string.main_particulate_unit);
                line4 = wdm.context.getResources().getString(R.string.widget_particles2) + " " + data2.sensordataValue[1] + " " + wdm.context.getResources().getString(R.string.main_particulate_unit);
            }
            if (data2.getSensorType() == SensorDataSet.TEMPERATURE_SENSOR) {
                line3 = wdm.context.getResources().getString(R.string.widget_temperature) + " " + data2.sensordataValue[0] + " " + wdm.context.getResources().getString(R.string.main_temperature_unit);
                line4 = wdm.context.getResources().getString(R.string.widget_humidity) + " " + data2.sensordataValue[1] + " " + wdm.context.getResources().getString(R.string.main_humidity_unit);
            }
        }
        float widget_textsize = widget_textsize_normal;
        Paint textpaint = new Paint();
        textpaint.setTextSize(wdm.getFontHeightInPixels(widget_textsize));
        float maxlinewidth = 0;
        String longestlinestring = "";
        if (line1 != null) {
            if (textpaint.measureText(line1) > maxlinewidth){
                maxlinewidth = textpaint.measureText(line1);
                longestlinestring = line1;
            }
        }
        if (line2 != null) {
            if (textpaint.measureText(line2) > maxlinewidth){
                maxlinewidth = textpaint.measureText(line2);
                longestlinestring = line2;
            }
        }
        if (line3 != null) {
            if (textpaint.measureText(line3) > maxlinewidth){
                maxlinewidth = textpaint.measureText(line3);
                longestlinestring = line3;
            }
        }
        if (line4 != null) {
            if (textpaint.measureText(line4) > maxlinewidth){
                maxlinewidth = textpaint.measureText(line4);
                longestlinestring = line4;
            }
        }
        while (textpaint.measureText(longestlinestring)>wdm.getWidgetWidth()-5){
            widget_textsize = widget_textsize - 1;
            textpaint.setTextSize(wdm.getFontHeightInPixels(widget_textsize));
        }

        if (time != 0){
            remoteViews.setTextViewTextSize(time, TypedValue.COMPLEX_UNIT_SP,widget_textsize);
            remoteViews.setTextColor(time,widget_textcolor);
            if ((time1 != null) && (time2 != null)){
                remoteViews.setViewVisibility(time, View.VISIBLE);
                remoteViews.setTextViewText(time, time1 + " / " + time2);
            } else {
                if (time1 != null){
                    remoteViews.setViewVisibility(time, View.VISIBLE);
                    remoteViews.setTextViewText(time, time1);
                }
                if (time2 != null){
                    remoteViews.setViewVisibility(time, View.VISIBLE);
                    remoteViews.setTextViewText(time, time2);
                }
            }
            if ((time1 == null) && (time2 == null))
                    remoteViews.setViewVisibility(time, View.GONE);
        }

        if ((sensor1_data1 != 0) && (line1 != null)){
            remoteViews.setTextViewTextSize(sensor1_data1, TypedValue.COMPLEX_UNIT_SP,widget_textsize);
            remoteViews.setTextColor(sensor1_data1,widget_textcolor);
            remoteViews.setViewVisibility(sensor1_data1, View.VISIBLE);
            remoteViews.setTextViewText(sensor1_data1, line1);
        } else {
            remoteViews.setViewVisibility(sensor1_data1, View.GONE);
        }

        if ((sensor1_data2 != 0) && (line2 != null)){
            remoteViews.setTextViewTextSize(sensor1_data2, TypedValue.COMPLEX_UNIT_SP,widget_textsize);
            remoteViews.setTextColor(sensor1_data2,widget_textcolor);
            remoteViews.setViewVisibility(sensor1_data2, View.VISIBLE);
            remoteViews.setTextViewText(sensor1_data2, line2);
        } else {
            remoteViews.setViewVisibility(sensor1_data2, View.GONE);
        }

        if ((sensor2_data1 != 0) && (line3 != null)){
            remoteViews.setTextViewTextSize(sensor2_data1, TypedValue.COMPLEX_UNIT_SP,widget_textsize);
            remoteViews.setTextColor(sensor2_data1,widget_textcolor);
            remoteViews.setViewVisibility(sensor2_data1, View.VISIBLE);
            remoteViews.setTextViewText(sensor2_data1, line3);
        } else {
            remoteViews.setViewVisibility(sensor2_data1, View.GONE);
        }
        if ((sensor2_data2 != 0) && (line4 != null)){
            remoteViews.setTextViewTextSize(sensor2_data2, TypedValue.COMPLEX_UNIT_SP,widget_textsize);
            remoteViews.setTextColor(sensor2_data2,widget_textcolor);
            remoteViews.setViewVisibility(sensor2_data2, View.VISIBLE);
            remoteViews.setTextViewText(sensor2_data2, line4);
        } else {
            remoteViews.setViewVisibility(sensor2_data2, View.GONE);
        }
        if (display_reference){
            remoteViews.setViewVisibility(R.id.widget_reference_text, View.VISIBLE);
            remoteViews.setTextColor(R.id.widget_reference_text,widget_textcolor);
        } else {
            remoteViews.setViewVisibility(R.id.widget_reference_text, View.GONE);
        }
    }

    /**
     * Creates a chart with particulate data.
     *
     * @param data the data list to display
     * @param width the width of the chart in pixels. Must not be 0.
     * @param height the height of the chart in pixels. Must not be 0.
     * @return bitmap with chart. May return null if width or height was null.
     */

    public Bitmap getParticulateChart(Context c, ArrayList<SensorDataSet> data, float width, float height){
        if ((height == 0) || (width == 0))
            return null;
        float radius = width / (float) 200;
        float connline_width = radius / 2;
        if (height>width)
            radius = height / (float) 200;
        if (radius<3)
            radius = 3;
        if (connline_width<3)
            connline_width=3;
        Long start_time = data.get(0).timestamp_UTCmillis;
        if (data.size()>1)
            start_time = data.get(data.size()-1).timestamp_UTCmillis;
       Long stop_time = data.get(0).timestamp_UTCmillis;
       float time_graphscale = (stop_time - start_time) / width;
       float max_value10 = 0;
       float max_value25 = 0;
       for (int i = 0; i < data.size(); i++) {
       if (data.get(i).getSensorValueFloat(0) > max_value10)
          max_value10 = data.get(i).getSensorValueFloat(0);
          if (data.get(i).getSensorValueFloat(1) > max_value25)
          max_value25 = data.get(i).getSensorValueFloat(1);
       }
       int display_steps = 5;
       if (height < 100)
          display_steps = 3;
       float font_reference_margin = height;
       if (width<height){
           font_reference_margin = width;
       }
       float fontsize = (float) (font_reference_margin / display_steps / 2);
       float max_particulate_displayvalue = max_value10;
       try {
            String s_digits = String.valueOf(max_value10);
            String r_digits = s_digits.substring(0,s_digits.indexOf("."));
            max_particulate_displayvalue = (float) Math.pow(10,r_digits.length());
       } catch (Exception e){
           max_particulate_displayvalue = max_value10;
       }
       if (max_value10 < 1000) {
            max_particulate_displayvalue = 1000;
       }
        if (max_value10 < 500) {
           max_particulate_displayvalue = 500;
       }
       if (max_value10 < 200) {
          max_particulate_displayvalue = 200;
       }
       if (max_value10 < 100) {
          max_particulate_displayvalue = 100;
       }
       if (max_value10 < 50) {
          max_particulate_displayvalue = 50;
       }
       if (max_value10 < 25) {
          max_particulate_displayvalue = 25;
       }
       float particulate_graphscale = max_particulate_displayvalue / height;
       Bitmap image = Bitmap.createBitmap(Math.round(width), Math.round(height), Bitmap.Config.ARGB_8888);
       Canvas canvas = new Canvas();
       canvas.setBitmap(image);
       Paint paint10 = new Paint();
       paint10.setStrokeWidth(connline_width);
       paint10.setStyle(Paint.Style.FILL_AND_STROKE);
       paint10.setAntiAlias(true);
       Paint paint25 = new Paint();
       paint25.setStrokeWidth(connline_width);
       paint25.setStyle(Paint.Style.FILL_AND_STROKE);
       paint25.setAntiAlias(true);
       Paint paint_i = new Paint();
       paint_i.setTextSize(fontsize);
       paint_i.setStyle(Paint.Style.FILL);
       paint_i.setAntiAlias(true);
       Paint paint_l = new Paint();
       Paint paint_hours = new Paint();
       paint_hours.setTextSize(fontsize / 2);
       paint_hours.setStyle(Paint.Style.FILL);
       paint_hours.setAntiAlias(true);
        if (widget_colorTheme == WIDGET_COLOR_THEME_DARK){
           paint_l.setColor(Color.DKGRAY);
           paint_i.setColor(Color.DKGRAY);
           paint_hours.setColor(Color.DKGRAY);
           paint25.setColor(Color.YELLOW);
           paint10.setColor(Color.GREEN);
       } else {
           paint_l.setColor(Color.WHITE);
           paint_i.setColor(Color.WHITE);
           paint_hours.setColor(Color.WHITE);
           paint25.setColor(Color.YELLOW);
           paint10.setColor(Color.GREEN);
       }
       paint_l.setStrokeWidth(3);
       paint_l.setStyle(Paint.Style.STROKE);
       paint_l.setStrokeWidth(1);
       paint_l.setPathEffect(new DashPathEffect(new float[]{2, 4, 2, 4}, 0));
       paint_l.setAntiAlias(true);
       if (data.size()>1){
           for (int i = 0; i < data.size() - 1; i++) {
               float x1 = (data.get(i).getTimestampUTCMillis() - start_time) / time_graphscale;
               float x2 = (data.get(i + 1).getTimestampUTCMillis() - start_time) / time_graphscale;
               float y1_10 = height - (data.get(i).getSensorValueFloat(0)) / particulate_graphscale;
               float y2_10 = height - (data.get(i + 1).getSensorValueFloat(0)) / particulate_graphscale;
               float y1_25 = height - (data.get(i).getSensorValueFloat(1)) / particulate_graphscale;
               float y2_25 = height - (data.get(i + 1).getSensorValueFloat(1)) / particulate_graphscale;
               if ((paint_lines) && (data.get(i).getTimestampUTCMillis()-(data.get(i + 1).getTimestampUTCMillis())<HOURS2_IN_MILLIS)){
                   canvas.drawLine(x1, y1_10, x2, y2_10, paint10);
                   canvas.drawLine(x1, y1_25, x2, y2_25, paint25);
               } else {
                   canvas.drawCircle(x1, y1_10, radius,paint10);
                   canvas.drawCircle(x1, y1_25, radius,paint25);
               }
            }
       }
       float line_width = width / 50;
       for (int i = 1; i <= display_steps; i++) {
           String s = String.valueOf((int) max_particulate_displayvalue / display_steps * i);
           if ((i == display_steps) && (display_units))
               s = s + " "+c.getResources().getString(R.string.main_particulate_unit);
           float x1 = 0;
           float x2 = width;
           float y1 = max_particulate_displayvalue / particulate_graphscale - (max_particulate_displayvalue / display_steps * i / particulate_graphscale);
           canvas.drawLine(x1, y1, x2, y1, paint_l);
           canvas.drawText(s, x1 + line_width + line_width / 10, y1 + fontsize, paint_i);
       }
       int hours = (int) (stop_time - start_time) / (60 * 60 * 1000);
       int minutes = (int) ((stop_time - start_time) % (60 * 60 * 1000)) / (60 * 1000);
       String span = "t = " + hours + "h " + minutes + "min";
       float span_width = paint_i.measureText(span);
       canvas.drawText(span, width - span_width, (float) (fontsize * 2), paint_i);
       if (label_x_axis){
           float hours_available = (stop_time - start_time) / (1000*60*60);
           float millis_shift = (stop_time - start_time) % (1000*60*60);
           float hours_line_length;
           Calendar cal = Calendar.getInstance();
           cal.setTimeInMillis(data.get(0).getTimestampLocalMillis());
           for (int i = Math.round(hours_available); i>0; i--){
               float time_x = (i * (1000*60*60) - millis_shift) / time_graphscale;
               hours_line_length = height / 20;
               paint_hours.setStrokeWidth(1);
               int hour = cal.get(Calendar.HOUR_OF_DAY);
               if ((hour == 4) || (hour== 8) || (hour==12) || (hour==16) || (hour==20) || (hour==0)) {
                   paint_hours.setStrokeWidth(3);
                   hours_line_length = height / 15;
                   String timelabel = String.valueOf(hour) + ":00";
                   float timelabel_width = paint_hours.measureText(timelabel);
                   canvas.drawText(timelabel, time_x - timelabel_width / 2, height - hours_line_length - fontsize/8, paint_hours);
               }
               canvas.drawLine(time_x,height,time_x, height- hours_line_length,paint_hours);
               cal.add(Calendar.HOUR,-1);
           }
       }
       return image;
    }

    /**
     * Generates the particulate matter chart from data and displays it in the imageview.
     *
     * @param data
     * @param imageview
     */

    public void drawParticulate(Context c, ArrayList<SensorDataSet> data, ImageView imageview){
        imageview.setImageBitmap(getParticulateChart(c,data,imageview.getWidth(),imageview.getHeight()));
    }

    /**
     * Generates the particulate matter chart from data and displays it in the imageview.
     * This is the version for widgets and it handles the actions via remoteviews.
     *
     * @param remoteViews
     * @param viewID
     * @param data
     * @param width
     * @param height
     */

    public void drawParticulate(Context c, RemoteViews remoteViews, int viewID, ArrayList<SensorDataSet> data, float width, float height){
        remoteViews.setImageViewBitmap(viewID, getParticulateChart(c,data,width,height));
    }

    /**
     * Creates a chart with temperature and humidity data.
     *
     * @param data the data list to display
     * @param width the width of the chart in pixels. Must not be 0.
     * @param height the height of the chart in pixels. Must not be 0.
     * @return bitmap with chart. May return null if width or height was null.
     */

    public Bitmap getTemperatureChart(Context c, ArrayList<SensorDataSet> data, float width, float height) {
        if (data == null)
            return null;
        if (data.size()==0)
            return null;
        if ((height == 0) || (width == 0))
            return null;
        Long start_time = data.get(0).timestamp_UTCmillis;
        if (data.size()>1)
            start_time = data.get(data.size()-1).timestamp_UTCmillis;
        Long stop_time = data.get(0).timestamp_UTCmillis;
        float time_graphscale = (stop_time - start_time) / width;
        float max_temp = 0;
        float min_temp = 0;
        float radius = width / (float) 200;
        float connline_width = radius / 2;
        if (height>width)
            radius = height / (float) 200;
        if (radius<3)
            radius = 3;
        if (connline_width<3)
            connline_width=3;
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getSensorValueFloat(0) > max_temp)
               max_temp = data.get(i).getSensorValueFloat(0);
            if (data.get(i).getSensorValueFloat(0) < min_temp)
               min_temp = data.get(i).getSensorValueFloat(0);
            }
        float delta_temp = max_temp - min_temp;
        if (min_temp>=0){
            delta_temp = max_temp;
        }
        int display_steps = 5;
        if (height < 100)
           display_steps = 3;
        float font_reference_margin = height;
        if (width<height){
            font_reference_margin = width;
        }
        float fontsize = (float) (font_reference_margin / display_steps / 2);
        float temp_scale_step_value = 20;
        if (delta_temp / display_steps < 10)
            temp_scale_step_value = 10;
        if (delta_temp / display_steps < 5)
            temp_scale_step_value = 5;
        float temp_bottom_offset_value = 0;
        if (min_temp < 0)
            temp_bottom_offset_value = -(((int) Math.abs(min_temp)/ (int) temp_scale_step_value)+1)*temp_scale_step_value;
        float temp_graphscale = temp_scale_step_value * display_steps / height;
        float zeroline_position = height;
        if (temp_bottom_offset_value != 0)
            zeroline_position = height + temp_bottom_offset_value / temp_graphscale;
        float scale_humidity = (float) 100 / height;
        Bitmap image = Bitmap.createBitmap(Math.round(width), Math.round(height), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas();
        canvas.setBitmap(image);
        Paint paint_zero = new Paint();
        paint_zero.setStrokeWidth(3);
        paint_zero.setStyle(Paint.Style.STROKE);
        paint_zero.setAntiAlias(true);
        Paint paint_t = new Paint();
        paint_t.setStrokeWidth(connline_width);
        paint_t.setStyle(Paint.Style.FILL_AND_STROKE);
        paint_t.setAntiAlias(true);
        Paint paint_h = new Paint();
        paint_h.setStrokeWidth(connline_width);
        paint_h.setStyle(Paint.Style.FILL_AND_STROKE);
        paint_h.setAntiAlias(true);
        Paint paint_i = new Paint();
        paint_i.setTextSize(fontsize);
        paint_i.setStyle(Paint.Style.FILL);
        paint_i.setAntiAlias(true);
        Paint paint_l = new Paint();
        paint_l.setStrokeWidth(3);
        paint_l.setStyle(Paint.Style.STROKE);
        paint_l.setStrokeWidth(1);
        paint_l.setPathEffect(new DashPathEffect(new float[]{2,4,2,4},0));
        paint_l.setAntiAlias(true);
        Paint paint_hours = new Paint();
        paint_hours.setTextSize(fontsize / 2);
        paint_hours.setStyle(Paint.Style.FILL);
        paint_hours.setAntiAlias(true);
        if (widget_colorTheme == WIDGET_COLOR_THEME_DARK){
            paint_zero.setColor(Color.CYAN);
            paint_t.setColor(Color.RED);
            paint_i.setColor(Color.DKGRAY);
            paint_h.setColor(Color.BLUE);
            paint_l.setColor(Color.DKGRAY);
            paint_hours.setColor(Color.DKGRAY);
        } else {
            paint_zero.setColor(Color.CYAN);
            paint_t.setColor(Color.RED);
            paint_i.setColor(Color.WHITE);
            paint_h.setColor(Color.BLUE);
            paint_l.setColor(Color.WHITE);
            paint_hours.setColor(Color.WHITE);
        }
        if (data.size()>1) {
            for (int i = 0; i < data.size() - 1; i++) {
                float x1 = (data.get(i).getTimestampUTCMillis() - start_time) / time_graphscale;
                float x2 = (data.get(i + 1).getTimestampUTCMillis() - start_time) / time_graphscale;
                if (data.get(i).getSensorValueFloat(0)<0) {
                    paint_t.setColor(Color.CYAN);
                } else {
                    paint_t.setColor(Color.RED);
                }
                float y1_t = zeroline_position - data.get(i).getSensorValueFloat(0) / temp_graphscale;
                float y2_t = zeroline_position - data.get(i + 1).getSensorValueFloat(0) / temp_graphscale;
                float y1_h = height - (data.get(i).getSensorValueFloat(1)) / scale_humidity;
                float y2_h = height - (data.get(i + 1).getSensorValueFloat(1)) / scale_humidity;
                if ((paint_lines) && (data.get(i).getTimestampUTCMillis()-(data.get(i + 1).getTimestampUTCMillis())<HOURS2_IN_MILLIS)){
                    canvas.drawLine(x1, y1_t, x2, y2_t, paint_t);
                    canvas.drawLine(x1, y1_h, x2, y2_h, paint_h);
                } else {
                    canvas.drawCircle(x1,y1_t,radius,paint_t);
                    canvas.drawCircle(x1,y1_h,radius,paint_h);
                }
            }
        }
        float line_width = width / 50;
        for (int i=1; i<=display_steps; i++){
            String s = String.valueOf(100/display_steps*i);
            String s2 = String.valueOf((int) (temp_bottom_offset_value+temp_scale_step_value*i));
            if ((i == display_steps) && (display_units)){
                s = s + c.getResources().getString(R.string.main_humidity_unit);
                s2 = s2 + c.getResources().getString(R.string.main_temperature_unit);
            }
            float x1 = 0;
            float x2 = width;
            float y1 = 100 / scale_humidity - (100/display_steps*i/scale_humidity);
            canvas.drawLine(x1,y1,x2,y1,paint_l);
            canvas.drawText(s,width - (x1+paint_i.measureText(s)+line_width+line_width/10),y1+fontsize,paint_i);
            canvas.drawText(s2,x1+line_width+line_width/10,y1+fontsize,paint_i);
        }
        int hours = (int) (stop_time - start_time) / (60*60*1000);
        int minutes = (int) ((stop_time - start_time) % (60*60*1000)) / (60*1000);
        String span = "t = "+ hours + "h "+minutes+ "min";
        String span2 = " 100";
        if (display_units)
            span2 = span2 + c.getResources().getString(R.string.main_humidity_unit);
        float span_width = paint_i.measureText(span) + paint_i.measureText(span2);
        canvas.drawText(span,width-span_width - line_width - line_width/10,(float) (fontsize * 2) ,paint_i);
        if (label_x_axis){
            float hours_available = (stop_time - start_time) / (1000*60*60);
            float millis_shift = (stop_time - start_time) % (1000*60*60);
            float hours_line_length;
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(data.get(0).getTimestampLocalMillis());
            for (int i = Math.round(hours_available); i>0; i--){
                float time_x = (i * (1000*60*60) - millis_shift) / time_graphscale;
                hours_line_length = height / 20;
                paint_hours.setStrokeWidth(1);
                int hour = cal.get(Calendar.HOUR_OF_DAY);
                if ((hour == 4) || (hour== 8) || (hour==12) || (hour==16) || (hour==20) || (hour==0)) {
                    paint_hours.setStrokeWidth(3);
                    hours_line_length = height / 15;
                    String timelabel = String.valueOf(hour) + ":00";
                    float timelabel_width = paint_hours.measureText(timelabel);
                    canvas.drawText(timelabel, time_x - timelabel_width / 2, height - hours_line_length - fontsize/8, paint_hours);
                }
                canvas.drawLine(time_x,height,time_x, height- hours_line_length,paint_hours);
                cal.add(Calendar.HOUR,-1);
            }
        }
        return image;
    }

    /**
     * Generates the temperature & humidity chart from data and displays it in the imageview.
     *
     * @param data
     * @param imageview
     */

    public void drawTemperature(Context c, ArrayList<SensorDataSet> data, ImageView imageview){
        imageview.setImageBitmap(getTemperatureChart(c, data,imageview.getWidth(),imageview.getHeight()));
    }

    /**
     * Generates the temperature & humidity chart from data and displays it in the imageview.
     * This is the version for widgets and it handles the actions via remoteviews.
     *
     * @param remoteViews
     * @param viewID
     * @param data
     * @param width
     * @param height
     */

    public void drawTemperature(Context c, RemoteViews remoteViews, int viewID, ArrayList<SensorDataSet> data, float width, float height){
        remoteViews.setImageViewBitmap(viewID, getTemperatureChart(c,data,width,height));
    }

    private Float getMaximumDistance(ArrayList<LocationDataSet> locations){
        float maxdistance = 0;
        for (int i=0; i<locations.size(); i++){
            LocationDataSet l = locations.get(i);
            if (l.distance>maxdistance){
                maxdistance = l.distance;
            }
        }
        return maxdistance;
    }

    public class CloseParticulateSensorsMap{

        public static final int NO_KEY = 0;
        public static final int LINEAR_KEY = 1;
        public static final int SHORT_KEY  = 2;
        public static final int POLLUTIONDISPLAY_NORMAL = 0;
        public static final int POLLUTIONDISPLAY_STRICT = 1;

        private ArrayList<LocationDataSet> locations;
        private ArrayList<SensorDataSet> sensordata;
        public Float margin;
        public int max_visible_concentration = 550;
        public int keytype = LINEAR_KEY;
        public int pollution_display = POLLUTIONDISPLAY_STRICT;
        public Boolean label_sensors_with_number = true;
        public Boolean drawReferenceLayer = false;
        public Boolean drawDistanceKey = false;
        public float zoom = (float) 1;
        float scale_x;
        float scale_y;

        public CloseParticulateSensorsMap(ArrayList<LocationDataSet> locations_source, ArrayList<SensorDataSet> senordata_source){
            locations  = new ArrayList<LocationDataSet>();
            sensordata   = new ArrayList<SensorDataSet>();
            for (int i=0; i<locations_source.size(); i++) {
                LocationDataSet l = locations_source.get(i);
                SensorDataSet s = senordata_source.get(i);
                if ((!s.sensordataValue[0].equals("")) && (!s.sensordataValue[1].equals(""))) {
                    locations.add(l);
                    sensordata.add(s);
                }
            }
        }

        public void setMaxVisibleConcentration(int max){
            max_visible_concentration = max;
        }

        public void setPollutionDisplay(int max){
            pollution_display = max;
        }

        public void setImageView(ImageView imageView){
            margin = (float) (imageView.getHeight() / 10);
            imageView.setImageBitmap(getNextSensorsMap(imageView.getWidth(),imageView.getHeight(),margin,true));

        }

        public void setKeyType(int i){
            keytype = i;
        }

        public void setZoom(float f){
            this.zoom = f;
        }

        private ArrayList<PointF> calculateSensorCoordinates(ArrayList<LocationDataSet> locations, float width, float height, float margin, boolean symmetric){
            ArrayList<Float> distance_x = new ArrayList<Float>();
            ArrayList<Float> distance_y = new ArrayList<Float>();
            for (int i=0; i<locations.size(); i++){
                LocationDataSet l = locations.get(i);
                Double dist_x = (Math.sin(Math.toRadians(l.bearing-90)) * l.distance);
                Double dist_y = (Math.cos(Math.toRadians(l.bearing-90)) * l.distance);
                distance_x.add((float) (Math.sin(Math.toRadians(l.bearing)) * l.distance));
                distance_y.add((float) (Math.cos(Math.toRadians(l.bearing)) * l.distance));
            }
            float maxdistance_x = 0;
            float maxdistance_y = 0;
            for (int i=0; i<distance_x.size(); i++){
                Float dx = Math.abs(distance_x.get(i));
                Float dy = Math.abs(distance_y.get(i));
                if (dx>maxdistance_x){
                    maxdistance_x = dx;
                }
                if (dy>maxdistance_y){
                    maxdistance_y = dy;
                }
            }
            scale_x = ((width-margin*2)  / maxdistance_x / 2);
            scale_y = ((height-margin*2) / maxdistance_y / 2);
            if (symmetric){
                if (scale_x<scale_y){
                    scale_y = scale_x;
                } else {
                    scale_x = scale_y;
                }
            }
            ArrayList<PointF> points = new ArrayList<PointF>();
            for (int i=0; i<distance_x.size(); i++){
                PointF p = new PointF();
                p.set(width/2+distance_x.get(i)*scale_x,height/2-distance_y.get(i)*scale_y);
                points.add(p);
            }
            return points;
        }

        public class RGBColor{
            int red = 0;
            int green = 0;
            int blue = 0;

            public RGBColor(int r, int g, int b){
                this.red = r;
                this.green = g;
                this.blue = b;
            }

            public int getIntColor(){
                return Color.rgb(red,green,blue);
            }
        }

        private RGBColor calculateParticulateColor(Double v){
            int red = 0;
            int green = 0;
            int blue = 0;
            if (pollution_display == POLLUTIONDISPLAY_STRICT){
                int value = (int) Math.round(v);
                if (value<50){
                    // green 0-50
                    green = (int) ((50 - value) * 5);
                    if (green < 0)
                        green = 0;
                }
                if (value>10){
                    // red: 10 to 100;
                    red = (int) (value*5-50);
                    if (red > 255)
                        red = 255;
                }
                if (value>100){
                    // blue: 200-455
                    blue = value-100;
                    if (blue>255)
                        blue = 255;
                }
            } else {
                int value = (int) Math.round(v);
                if (value<100){
                    // green 0-100
                    green = (int) ((100 - value) * 2.5);
                    if (green < 0)
                        green = 0;
                }
                if (value>15){
                    // red: 15 to 200;
                    red = (int) (value*1.38);
                    if (red > 255)
                        red = 255;
                }
                if (value>200){
                    // blue: 200-455
                    blue = value-200;
                    if (blue>255)
                        blue = 255;
                }

            }
            RGBColor c = new RGBColor(red,green,blue);
            return c;
        }

        public RGBColor calculateParticulateColor(int value){
            return calculateParticulateColor((double) value);
        }

        public RGBColor calculateParticulateColor(float value){
            return calculateParticulateColor((double) value);
        }

        private void drawLabelOnCanvas(Canvas c, int value, float margin, float ypos, Paint p){
            c.drawText(String.valueOf(value) + " µg/m³", margin / 2 + 22, ypos, p);
        }

        public void drawReferenceLayer(Boolean b){
            drawReferenceLayer = b;
        }

        public void drawReferenceLayer(){
            drawReferenceLayer = true;
        }

        private String getMaxPollution(int value){
            float maxvalue = 0;
            float v = 0;
            for (int i=0; i<sensordata.size(); i++){
                v = sensordata.get(i).getSensorValueFloat(value);
                if (v>maxvalue)
                    maxvalue = v;
                }
            return String.valueOf(v)+ " µg/m³";
        }

        private void drawDistanceKey(Canvas canvas, float width, float hight){
            float keyhight = hight / 20;
            float keysize = (float) 0.40;
            String scaletext = String.valueOf((Math.round((width * keysize) / scale_x)))+" m";
            Paint keypaint = new Paint();
            keypaint.setColor(Color.WHITE);
            keypaint.setStyle(Paint.Style.FILL);
            keypaint.setTextSize((float) (keyhight*0.75));
            keypaint.setAntiAlias(true);
            keypaint.setStrokeWidth(2);
            canvas.drawLine(width - width*keysize,0,width - width*keysize,keyhight,keypaint);
            canvas.drawLine(width,0,width,keyhight,keypaint);
            canvas.drawLine(width - width*keysize,keyhight/2,width,keyhight/2,keypaint);
            canvas.drawText(scaletext,width - width*(keysize/2)-keypaint.measureText(scaletext)/2,keyhight+4,keypaint);
            float textsize_max = (float) (keypaint.getTextSize()*2);
            keypaint.setTextSize(textsize_max);
            String p1 ="P10 max =" + getMaxPollution(0);
            String p2 ="P2.5 max = " + getMaxPollution(1);
            while ((keypaint.measureText(p1)>width/2) || (keypaint.measureText(p2)>width/2))
                keypaint.setTextSize((float) (keypaint.getTextSize()*0.9));
            canvas.drawText(p1,0,textsize_max,keypaint);
            canvas.drawText(p2,0,textsize_max*2,keypaint);
        }

        private PointF getValuesOnCanvasTextOffset(ArrayList<PointF> values, float width, float height){
            PointF offset = new PointF(0,0);
            int left_top = 0;
            int left_bottom = 0;
            int right_top = 0;
            int right_bottom = 0;
            for (int i=0; i<values.size(); i++){
                if (values.get(i).x < width/2){
                    if (values.get(i).y < height/2){
                        left_top = left_top + 1;
                    } else {
                        left_bottom = left_bottom + 1;
                    }
                } else {
                    if (values.get(i).y < height/2){
                        right_top = right_top + 1;
                    } else {
                        right_bottom = right_bottom + 1;
                    }
                }
            }
            if ((right_bottom<=right_top) && (right_bottom<=left_top) && (right_bottom<=left_bottom)){
                // right bottom
                offset.x = width / 2;
                offset.y = height / 2;
            } else if ((right_top<=left_top) && (right_top<=left_bottom) && (right_top<=right_bottom)){
                //right top
                offset.x = width / 2;
                offset.y = 0;
            } else if ((left_bottom<=left_top) && (left_bottom<=right_bottom) && (left_bottom<=right_top)){
                // left bottom
                offset.x = 0;
                offset.y = height / 2;
            }
            return offset;
        }

        private void drawValuesOnCanvas(Canvas canvas, ArrayList<PointF> coordinates, ArrayList<SensorDataSet> sensors, float width, float height){
            PointF offset = getValuesOnCanvasTextOffset(coordinates,width,height);
            float maxvalue10 = 0;
            float maxvalue25 = 0;

            Paint paint = new Paint();
            float fontsize = height/20;
            paint.setTextSize(fontsize);
            // canvas.drawText("Hello, World!",offset.x,offset.y,paint);

        }

        public Bitmap getKeyForMap(float margin, float height){
            String text_template="550 µg/m³";
            float fontsize = height / 20;
            Paint sensorlabel = new Paint();
            sensorlabel.setTextSize(fontsize);
            sensorlabel.setStyle(Paint.Style.FILL);
            sensorlabel.setAntiAlias(true);
            sensorlabel.setColor(widget_textcolor);
            Bitmap key_bitmap = Bitmap.createBitmap(Math.round(margin/2) + (int) sensorlabel.measureText(text_template) + 42, Math.round(height), Bitmap.Config.ARGB_8888);
            Canvas key_canvas = new Canvas();
            key_canvas.setBitmap(key_bitmap);
            Paint linestyle = new Paint();
            linestyle.setAntiAlias(false);
            linestyle.setAlpha(255);
            if (keytype == LINEAR_KEY){
               float step = max_visible_concentration / height;
                linestyle.setStrokeWidth(step*2);
               float i = 0;
               while (i<height) {
                   RGBColor rgbColor = calculateParticulateColor(i);
                   linestyle.setColor(rgbColor.getIntColor());
                   key_canvas.drawLine(0, height-i, margin / 2, height-i, linestyle);
                   i = i + step;
               }
               // drawLabelOnCanvas(key_canvas,100,height-100/step,sensorlabel);
                for (int a=1; a<max_visible_concentration/50; a++){
                    key_canvas.drawLine(margin/2,height - a*50/step,margin/2 + 20, height - a*50/step,sensorlabel);
                    drawLabelOnCanvas(key_canvas,a*50,margin,height - a*50 / step + fontsize/2, sensorlabel);
                }
            } else {
                /*
                margin to top = height * 0.125
                .300
                .
                .200
                .
                .100    - offset2
                .
                .75
                .
                .50
                .
                .25
                .
                .0      - offset1
                margin to bottom = height*0.125
                 */
                float keyheight = (float) (height *0.75);
                float offset1   = (float) (height - height * 0.125);
                float offset2   = (float) (height - height * 0.125 - height*0.75*0.66);
                float step = max_visible_concentration / keyheight;
                linestyle.setStrokeWidth(step*2);
                linestyle.setStyle(Paint.Style.FILL_AND_STROKE);
                int i = 0;
                float x0 = 0;
                float x1 = 0;
                while (i<max_visible_concentration){
                    RGBColor rgbColor = calculateParticulateColor(i);
                    linestyle.setColor(rgbColor.getIntColor());
                    if (i<=100){
                        step = (float) (max_visible_concentration / (keyheight)*0.333);
                        x0 = offset1 -  i    / step;
                        x1 = offset1 - (i-1) / step;
                        offset2 = x0;
                    }
                    if (i>100){
                        step = (float) (max_visible_concentration / (keyheight)*0.666);
                        x0 = offset2 -  (i-100)    / step;
                        x1 = offset2 -  (i-101) / step;
                    }
                    key_canvas.drawRect(0,x0, margin / 2, x1,linestyle);
                    if ((i == 0) || (i == 25) || (i == 50) || (i == 75) || (i == 100) || (i == 200)|| (i == 300)) {
                        key_canvas.drawLine(margin / 2, x0, margin / 2 + 20, x0, sensorlabel);
                        key_canvas.drawText(String.valueOf(i) + " µg/m³", margin / 2 + 22, x0 + fontsize / 2, sensorlabel);
                    }
                    i = i + 1;
                }
            }
        return key_bitmap;
        }

        public Bitmap getNextSensorsMap(float width, float height, float margin,  boolean symmetric){
            if ((height == 0) || (width==0)){
                return null;
            }
            ArrayList<PointF> coordinates = calculateSensorCoordinates(locations,width,height,margin,symmetric);
            Bitmap image = Bitmap.createBitmap(Math.round(width), Math.round(height), Bitmap.Config.ARGB_8888);
            Float fontsize = height / 30;
            Canvas canvas = new Canvas();
            canvas.setBitmap(image);
            // circle style
            Paint circle = new Paint();
            circle.setStrokeWidth(1);
            circle.setStyle(Paint.Style.FILL_AND_STROKE);
            circle.setAntiAlias(true);
            circle.setColor(Color.GREEN);
            // Text style
            Paint sensorlabel = new Paint();
            sensorlabel.setTextSize(fontsize*zoom);
            sensorlabel.setStyle(Paint.Style.FILL);
            sensorlabel.setAntiAlias(true);
            sensorlabel.setColor(Color.WHITE);
            // draw the circles
            for (int i=0; i<locations.size(); i++){
                LocationDataSet l = locations.get(i);
                SensorDataSet   s = sensordata.get(i);
                PointF          p = coordinates.get(i);
                RGBColor rgbColor = calculateParticulateColor(s.getSensorValueInt(0));
                circle.setColor(rgbColor.getIntColor());
                circle.setAlpha(125);
                canvas.drawCircle(p.x,p.y,margin*zoom,circle);
                if (label_sensors_with_number){
                    canvas.drawText(l.number,p.x-sensorlabel.measureText(l.number)/2,p.y,sensorlabel);
                } else {
                    String label1 = s.sensordataValue[0]+" µg/m³";
                    String label2 = s.sensordataValue[1]+" µg/m³";
                    String label3 = Math.round(l.distance)+" m";
                    canvas.drawText(label1,p.x-sensorlabel.measureText(label1)/2, (float) (p.y-sensorlabel.getTextSize()*0.75),sensorlabel);
                    canvas.drawText(label2,p.x-sensorlabel.measureText(label2)/2, (float) (p.y+sensorlabel.getTextSize()*0.25),sensorlabel);
                    canvas.drawText(label3,p.x-sensorlabel.measureText(label3)/2, (float) (p.y+sensorlabel.getTextSize()*1.25),sensorlabel);
                }
            }
            // draw the center position (=own position)
            Paint centerpos = new Paint();
            centerpos.setStrokeWidth(1);
            centerpos.setStyle(Paint.Style.FILL_AND_STROKE);
            centerpos.setAntiAlias(true);
            centerpos.setColor(Color.WHITE);
            canvas.drawCircle(width/2,height/2,margin/4,centerpos);
            // draw the lines
            if (drawDistanceKey)
                drawDistanceKey(canvas,width,height);
            // draw the key on a seperate bitmap
            Bitmap key_bitmap = getKeyForMap(margin,height);
            Bitmap result_bitmap = Bitmap.createBitmap(image.getWidth() + key_bitmap.getWidth(), Math.round(height), Bitmap.Config.ARGB_8888);
            Canvas result_canvas = new Canvas();
            result_canvas.setBitmap(result_bitmap);
            result_canvas.drawBitmap(key_bitmap,0,0,null);
            result_canvas.drawBitmap(image,key_bitmap.getWidth(),0,null);
            // draw the reference layer
            if (drawReferenceLayer){
                drawReferenceLayerToBitmap(result_bitmap);
            }
            return result_bitmap;
        }
    }

    public class CloseSensorsTemperatureMap {

        private ArrayList<LocationDataSet> locations;
        private ArrayList<SensorDataSet> sensordata;

        public Boolean drawReferenceLayer = false;
        public Boolean display_sensor_number = true;
        public Boolean display_values_in_chart = true;

        public void drawReferenceLayer(Boolean b){
            drawReferenceLayer = b;
        }

        public void displaySensorNumber(Boolean b){
            display_sensor_number = b;
        }

        public void displayValuesInChart(Boolean b){
            display_values_in_chart = b;
        }

        public CloseSensorsTemperatureMap(ArrayList<LocationDataSet> locations_source, ArrayList<SensorDataSet> senordata_source){
            locations  = new ArrayList<LocationDataSet>();
            sensordata   = new ArrayList<SensorDataSet>();
            for (int i=0; i<locations_source.size(); i++) {
                LocationDataSet l = locations_source.get(i);
                SensorDataSet s = senordata_source.get(i);
                if ((!s.sensordataValue[0].equals("")) && (!s.sensordataValue[1].equals(""))) {
                    locations.add(l);
                    sensordata.add(s);
                }
            }
        }

        private float getMaxTemperature(){
            SensorDataSet s = sensordata.get(0);
            float maxtemperature = s.getSensorValueFloat(0);
            for (int i=0; i<sensordata.size(); i++){
                s = sensordata.get(i);
                if (s.getSensorValueFloat(0)>maxtemperature){
                    maxtemperature = s.getSensorValueFloat(0);
                }
            }
            return maxtemperature;
        }

        private float getMinTemperature(){
            SensorDataSet s = sensordata.get(0);
            float mintemperature = s.getSensorValueFloat(0);
            for (int i=0; i<sensordata.size(); i++){
                s = sensordata.get(i);
                if (s.getSensorValueFloat(0)<mintemperature){
                    mintemperature = s.getSensorValueFloat(0);
                }
            }
            return mintemperature;
        }

        private float getMaxHumidity(){
            SensorDataSet s = sensordata.get(0);
            float maxhumidity = s.getSensorValueFloat(1);
            for (int i=0; i<sensordata.size(); i++){
                s = sensordata.get(i);
                if (s.getSensorValueFloat(1)>maxhumidity){
                    maxhumidity = s.getSensorValueFloat(1);
                }
            }
            return maxhumidity;
        }

        private float getMinHumidity(){
            SensorDataSet s = sensordata.get(0);
            float minhumidity = s.getSensorValueFloat(1);
            for (int i=0; i<sensordata.size(); i++){
                s = sensordata.get(i);
                if (s.getSensorValueFloat(1)<minhumidity){
                    minhumidity = s.getSensorValueFloat(1);
                }
            }
            return minhumidity;
        }

        public Bitmap getCloseSensorsTemperatureChart(float width, float height) {
            if ((height == 0) || (width == 0))
                return null;
            float max_temp = getMaxTemperature();
            float min_temp = getMinTemperature();
            float radius = width / (float) 200;
            float connline_width = radius / 2;
            if (height>width)
                radius = height / (float) 200;
            if (radius<3)
                radius = 3;
            if (connline_width<3)
                connline_width=3;
            float delta_temp = max_temp - min_temp;
            if (min_temp>=0){
                delta_temp = max_temp;
            }
            int display_steps = 5;
            if (height < 100)
                display_steps = 3;
            float fontsize = height / display_steps / 4;
            float temp_scale_step_value = 20;
            if (delta_temp / display_steps < 10)
                temp_scale_step_value = 10;
            if (delta_temp / display_steps < 5)
                temp_scale_step_value = 5;
            float temp_bottom_offset_value = 0;
            if (min_temp < 0)
                temp_bottom_offset_value = -(((int) Math.abs(min_temp)/ (int) temp_scale_step_value)+1)*temp_scale_step_value;
            float temp_graphscale = temp_scale_step_value * display_steps / height;
            float zeroline_position = height;
            if (temp_bottom_offset_value != 0)
                zeroline_position = height + temp_bottom_offset_value / temp_graphscale;
            float scale_humidity = (float) 100 / height;
            Bitmap image = Bitmap.createBitmap(Math.round(width), Math.round(height), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas();
            canvas.setBitmap(image);
            Paint paint_zero = new Paint();
            paint_zero.setStrokeWidth(3);
            paint_zero.setStyle(Paint.Style.STROKE);
            paint_zero.setAntiAlias(true);
            Paint paint_t = new Paint();
            paint_t.setStrokeWidth(connline_width);
            paint_t.setStyle(Paint.Style.FILL_AND_STROKE);
            paint_t.setAntiAlias(true);
            Paint paint_h = new Paint();
            paint_h.setStrokeWidth(connline_width);
            paint_h.setStyle(Paint.Style.FILL_AND_STROKE);
            paint_h.setAntiAlias(true);
            Paint paint_i = new Paint();
            paint_i.setTextSize(fontsize);
            paint_i.setStyle(Paint.Style.FILL);
            paint_i.setAntiAlias(true);
            Paint paint_l = new Paint();
            paint_l.setStrokeWidth(3);
            paint_l.setStyle(Paint.Style.STROKE);
            paint_l.setStrokeWidth(1);
            paint_l.setPathEffect(new DashPathEffect(new float[]{2,4,2,4},0));
            paint_l.setAntiAlias(true);
            Paint paint_hours = new Paint();
            paint_hours.setTextSize(fontsize / 2);
            paint_hours.setStyle(Paint.Style.FILL);
            paint_hours.setAntiAlias(true);
            Paint paint_snl = new Paint();
            paint_snl.setTextSize(width/10 - 4);
            paint_snl.setStyle(Paint.Style.FILL);
            paint_snl.setAntiAlias(true);
            if (widget_colorTheme == WIDGET_COLOR_THEME_DARK){
                paint_zero.setColor(Color.CYAN);
                paint_t.setColor(Color.RED);
                paint_i.setColor(Color.DKGRAY);
                paint_h.setColor(Color.BLUE);
                paint_l.setColor(Color.DKGRAY);
                paint_hours.setColor(Color.DKGRAY);
                paint_snl.setColor(Color.DKGRAY);
            } else {
                paint_zero.setColor(Color.CYAN);
                paint_t.setColor(Color.RED);
                paint_i.setColor(Color.WHITE);
                paint_h.setColor(Color.BLUE);
                paint_l.setColor(Color.WHITE);
                paint_hours.setColor(Color.WHITE);
                paint_snl.setColor(Color.WHITE);
            }
            float numberlabel_width;
            for (int i=0; i<sensordata.size(); i++) {
                if (sensordata.get(i).getSensorValueFloat(0)<0) {
                    paint_t.setColor(Color.CYAN);
                } else {
                    paint_t.setColor(Color.RED);
                }
                float y1_t = zeroline_position - sensordata.get(i).getSensorValueFloat(0) / temp_graphscale;
                float y1_h = height - (sensordata.get(i).getSensorValueFloat(1)) / scale_humidity;
                float x1_t = (float) ((i*1.5+1) * width / 10);
                float x1_h = (float) ((i*1.5+1) * width / 10 + width/20);
                canvas.drawRect(x1_t, y1_t, x1_t+width/20, zeroline_position, paint_t);
                canvas.drawRect(x1_h, y1_h, x1_h+width/20,height,paint_h);
                float ytext;
                // display sensor number
                if (display_sensor_number){
                    paint_snl.setTextSize(width/10 - 4);
                    numberlabel_width = paint_snl.measureText("12345");
                    while (numberlabel_width>width/10) {
                        paint_snl.setTextSize(paint_snl.getTextSize() - 1);
                        numberlabel_width = paint_snl.measureText("12345");
                    }
                    ytext = zeroline_position;
                    paint_snl.setColor(Color.WHITE);
                    if (sensordata.get(i).getSensorValueFloat(0)<0){
                        ytext = ytext - paint_snl.getTextSize();
                    }
                    canvas.drawText(sensordata.get(i).sensorId,x1_t + (width/10-paint_snl.measureText(sensordata.get(i).sensorId))/2, ytext,paint_snl);
                }
                // display temp value in chart
                if (display_values_in_chart){
                    paint_snl.setTextSize(width/20);
                    numberlabel_width = paint_snl.measureText("88.88");
                    while (numberlabel_width>width/20) {
                        paint_snl.setTextSize(paint_snl.getTextSize() - 1);
                        numberlabel_width = paint_snl.measureText("88.88");
                    }
                    if (sensordata.get(i).getSensorValueFloat(0)>0){
                        ytext = y1_t + paint_snl.getTextSize();
                    } else {
                        ytext = y1_t;
                        paint_snl.setColor(Color.DKGRAY);
                    }
                    canvas.drawText(sensordata.get(i).sensordataValue[0],x1_t + (width/20-paint_snl.measureText(sensordata.get(i).sensordataValue[0]))/2,ytext,paint_snl);
                    // display humidity value in chart
                    ytext = y1_h + paint_snl.getTextSize();
                    paint_snl.setColor(Color.WHITE);
                    canvas.drawText(sensordata.get(i).sensordataValue[1],x1_h + (width/20-paint_snl.measureText(sensordata.get(i).sensordataValue[1]))/2,ytext,paint_snl);
                }
            }
            for (int i=1; i<=display_steps; i++){
                String s = String.valueOf(100/display_steps*i);
                String s2 = String.valueOf(temp_bottom_offset_value+temp_scale_step_value*i);
                if ((i == display_steps) && (display_units)){
                    s = s + "%";
                    s2 = s2 + "°C";
                }
                float x1 = 0;
                float x2 = width;
                float y1 = 100 / scale_humidity - (100/display_steps*i/scale_humidity);
                canvas.drawLine(x1,y1,x2,y1,paint_l);
                // canvas.drawText(s,width - (x1+paint_i.measureText(s)+line_width+line_width/10),y1+fontsize,paint_i);
                canvas.drawText(s,width - (x1+paint_i.measureText(s)),y1+fontsize,paint_i);
                // canvas.drawText(s2,x1+line_width+line_width/10,y1+fontsize,paint_i);
                canvas.drawText(s2,0,y1+fontsize,paint_i);
            }
            if (drawReferenceLayer){
                drawReferenceLayerToBitmap(image);
            }
            return image;
        }
    }

    /**
     * Displays the map with the close sensors in an imageview.
     *
     * Purges the ArrayList to exclude sensors with empty values.
     *
     * @param locations_source ArrayList with the locations to display.
     * @param sensordata_source ArrayList with the corresponding sensor data.
     * @param width Width of the result bitmap in pixels
     * @param height Height of the result bitmap in pixels
     */

    public Bitmap getCloseSensorsMap(ArrayList<LocationDataSet> locations_source, ArrayList<SensorDataSet> sensordata_source, float width, float height, Boolean zoom){
        if ((locations_source == null) || (sensordata_source == null))
            return null;
        if ((locations_source.size()==0) || (sensordata_source.size() == 0))
            return null;
        ArrayList<LocationDataSet> locations  = new ArrayList<LocationDataSet>();
        ArrayList<SensorDataSet> sensordata   = new ArrayList<SensorDataSet>();
        for (int i=0; i<locations_source.size(); i++) {
            LocationDataSet l = locations_source.get(i);
            SensorDataSet s = sensordata_source.get(i);
            if ((!s.sensordataValue[0].equals("")) && (!s.sensordataValue[1].equals(""))) {
                locations.add(l);
                sensordata.add(s);
            }
        }
        SensorDataSet s = sensordata.get(0);
        if (s.getSensorType() == SensorDataSet.PARTICULATE_SENSOR){
            float margin = height / 10;
            CloseParticulateSensorsMap map = new CloseParticulateSensorsMap(locations,sensordata);
            map.setPollutionDisplay(CloseParticulateSensorsMap.POLLUTIONDISPLAY_STRICT);
            map.setKeyType(CloseParticulateSensorsMap.SHORT_KEY);
            if (zoom)
                map.setZoom(LARGE_ZOOM);
            return map.getNextSensorsMap(width,height,margin,true);
        } else {
            CloseSensorsTemperatureMap map = new CloseSensorsTemperatureMap(locations,sensordata);
            map.displaySensorNumber(true);
            map.displayValuesInChart(true);
            return map.getCloseSensorsTemperatureChart(width,height);
        }
    }

    public void drawCloseSensorsMap(ArrayList<LocationDataSet> locations_source, ArrayList<SensorDataSet> sensordata_source, ImageView imageView,Boolean zoom){
        if (imageView != null){
            imageView.setImageBitmap(getCloseSensorsMap(locations_source,sensordata_source, imageView.getWidth(), imageView.getHeight(),zoom));
            imageView.invalidate();
        }
    }

    public Bitmap getArrowBitmap(Context context, float degrees){
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),R.mipmap.arrow);
        if (bitmap != null){
            Matrix m = new Matrix();
            m.postRotate(degrees);
            return Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),m,false);
        } else {
            return null;
        }
    }

    public Bitmap getScaledArrowBitmap(Context context, float degrees){
        Bitmap bitmap = getArrowBitmap(context,degrees);
        if (bitmap != null){
            DisplayMetrics dm = new DisplayMetrics();
            dm = context.getResources().getDisplayMetrics();
            float fontsize = context.getResources().getDimension(R.dimen.closesensorstable_textsize);
            int size = Math.round(dm.scaledDensity*fontsize);
            return Bitmap.createScaledBitmap(bitmap,size,size,false);
        } else {
            return null;
        }
    }

    /**
     * Draws the reference on top of the bitmap.
     *
     * @param source
     */

    public void drawReferenceLayerToBitmap(Bitmap source){
        Paint background = new Paint();
        background.setColor(Color.WHITE);
        background.setStyle(Paint.Style.FILL_AND_STROKE);
        background.setAlpha(125);
        Paint text = new Paint();
        text.setTextSize(source.getHeight()/25);
        text.setColor(Color.BLACK);
        float x = source.getWidth() - SOURCEBORDER*2 - text.measureText(sourcetext);
        float y = source.getHeight() - SOURCEBORDER*2 - text.getTextSize();
        Canvas c = new Canvas();
        c.setBitmap(source);
        c.drawRect(x,y,source.getWidth()-SOURCEBORDER,source.getHeight()-SOURCEBORDER,background);
        c.drawText(sourcetext,x+SOURCEBORDER,y + text.getTextSize(),text);
    }

    /**
     * Generates a new bitmap with the reference under the source bitmap.
     *
     * @param source
     * @return
     */

    public Bitmap attachReferenceLayerToBitmap(Bitmap source){
        if (source != null){
            Paint text = new Paint();
            float textsize = REFERENCE_DEFAULT_SIZE;
            text.setColor(widget_textcolor);
            text.setTextSize(textsize);
            // increase text size if it is too tiny compared to the width of the bitmap.
            while (text.measureText(sourcetext)<source.getWidth()*0.5){
                textsize = textsize + 1;
                text.setTextSize(textsize);
            }
            // reduce textsize if too large and cannot be displayed
            while (text.measureText(sourcetext)>source.getWidth()){
                textsize = textsize - 1;
                text.setTextSize(textsize);
            }
            // generate a bitmap with the reference
            Bitmap referenceBitmap = Bitmap.createBitmap(source.getWidth(),Math.round(textsize+1), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas();
            c.setBitmap(referenceBitmap);
            c.drawText(sourcetext,referenceBitmap.getWidth()-text.measureText(sourcetext),referenceBitmap.getHeight(),text);
            // generate the result bitmap
            Bitmap result = Bitmap.createBitmap(source.getWidth(),Math.round(source.getHeight()+referenceBitmap.getHeight()), Bitmap.Config.ARGB_8888);
            Canvas result_canvas = new Canvas();
            result_canvas.setBitmap(result);
            result_canvas.drawBitmap(source,0,0,text);
            result_canvas.drawBitmap(referenceBitmap,0,source.getHeight(),text);
            return result;
        } else {
            return null;
        }
    }

    /**
     * Convenience method to get a string with the time from a location.
     */

    private String getLocalLocationTimeString(Location l){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(l.getTime());
        SimpleDateFormat simpleDateFormat_target = new SimpleDateFormat("dd.MM. HH:mm", Locale.getDefault());
        try{
            return simpleDateFormat_target.format(calendar.getTimeInMillis());
        } catch (Exception e){
            return "";
        }
    }

    /**
     * Convenience method to get a string with the current time.
     */

    private String getCurrentTimeString(){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat_target = new SimpleDateFormat("HH:mm", Locale.getDefault());
        try{
            return simpleDateFormat_target.format(calendar.getTimeInMillis());
        } catch (Exception e){
            return "";
        }
    }

    public RemoteViews displayCloseWidget(WidgetDimensionManager wdm, RemoteViews rv, int line1, int line2, int line3, int maxvalueline1, int maxvalueline2, int graph, Boolean onlytwolines, Location l, String l_source, ArrayList<LocationDataSet> location_data, ArrayList<SensorDataSet> sensor_data, int opacity, Boolean display_reference, Boolean zoom){
        float widget_textsize = widget_textsize_normal;
        // set widget opacity
        rv.setInt(R.id.widget_widget_close_relative,"setBackgroundColor",getBackgroundInt(opacity));
        DecimalFormat decimalFormat = new DecimalFormat();
        decimalFormat.applyPattern("###.###");
        String lat_string  = decimalFormat.format(l.getLatitude());
        String long_string = decimalFormat.format(l.getLongitude());
        // line1 text
        String line1text = wdm.context.getResources().getString(R.string.main_location_devicestatus_position_lat)+" "+lat_string + " " +
                wdm.context.getResources().getString(R.string.main_location_devicestatus_position_long)+" "+long_string + " "+
                "("+getLocalLocationTimeString(l)+" "+l_source+")";
        // line2 text
        String line2text = wdm.context.getResources().getString(R.string.main_location_lastupdate_time)+" "+getCurrentTimeString()+" ";
        // line3 text
        String line3text = "";
        if (location_data != null){
            if (location_data.size()>0){
                float max_distance = 0;
                for (int i=0; i<location_data.size(); i++){
                    if (location_data.get(i).distance>max_distance)
                        max_distance = location_data.get(i).distance;
                }
                line3text = wdm.context.getResources().getString(R.string.widget_maxdistance)+" "+String.valueOf(Math.round(max_distance)+" m");
            }
        }
        String line4text = "";
        String line5text = "";
        if (sensor_data != null){
            if (sensor_data.size()>1){
                // set max values to value #0 of arraylist
                int maxvalueindex1 = 0;
                int maxvalueindex2 = 0;
                float maxvalue1 = sensor_data.get(0).getSensorValueFloat(0);
                float maxvalue2 = sensor_data.get(0).getSensorValueFloat(1);
                String maxvaluestring1 = sensor_data.get(0).sensordataValue[0];
                String maxvaluestring2 = sensor_data.get(0).sensordataValue[1];
                // set min values to value #0 of arraylist
                int minvalueindex1 = 0;
                int minvalueindex2 = 0;
                float minvalue1 = sensor_data.get(0).getSensorValueFloat(0);
                float minvalue2 = sensor_data.get(0).getSensorValueFloat(1);
                String minvaluestring1 = sensor_data.get(0).sensordataValue[0];
                String minvaluestring2 = sensor_data.get(0).sensordataValue[1];
                /*
                 * Run through the arraylist to get the max and min values in it.
                 */
                for (int i=1; i<sensor_data.size(); i++) {
                    if (sensor_data.get(i).getSensorValueFloat(0) > maxvalue1) {
                        maxvalueindex1 = i;
                        maxvaluestring1 = sensor_data.get(i).sensordataValue[0];
                        maxvalue1 = sensor_data.get(i).getSensorValueFloat(0);
                    }
                    if (sensor_data.get(i).getSensorValueFloat(0) < minvalue1) {
                        minvalueindex1 = i;
                        minvaluestring1 = sensor_data.get(i).sensordataValue[0];
                        minvalue1 = sensor_data.get(i).getSensorValueFloat(0);
                    }
                    if (sensor_data.get(i).getSensorValueFloat(1) > maxvalue2) {
                        maxvalueindex2 = i;
                        maxvaluestring2 = sensor_data.get(i).sensordataValue[1];
                        maxvalue2 = sensor_data.get(i).getSensorValueFloat(1);
                    }
                    if (sensor_data.get(i).getSensorValueFloat(1) < minvalue2) {
                        minvalueindex2 = i;
                        minvaluestring2 = sensor_data.get(i).sensordataValue[1];
                        minvalue2 = sensor_data.get(i).getSensorValueFloat(1);
                    }
                }
                if (sensor_data.get(0).getSensorType() == SensorDataSet.PARTICULATE_SENSOR){
                    line4text = wdm.context.getResources().getString(R.string.widget_close_max25)+" "+maxvaluestring2+" "+wdm.context.getResources().getString(R.string.main_particulate_unit);
                    line5text = wdm.context.getResources().getString(R.string.widget_close_max10)+" "+maxvaluestring1+" "+wdm.context.getResources().getString(R.string.main_particulate_unit);
                } else {
                    line4text = wdm.context.getResources().getString(R.string.widget_close_temperature)+" "+minvaluestring1 + " \u2194 "+maxvaluestring1 + " "+wdm.context.getResources().getString(R.string.main_temperature_unit);
                    line5text = wdm.context.getResources().getString(R.string.widget_close_humidity)+" "+minvaluestring2 + " \u2194 "+maxvaluestring2 + " "+wdm.context.getResources().getString(R.string.main_humidity_unit);
                }
            }
        }
        /*
         * Determines the line with the max width and reduces text size if necessary to avoid
         * line breaks.
         */
        String longestline = line1text;
        Paint textpaint = new Paint();
        textpaint.setTextSize(wdm.getFontHeightInPixels(widget_textsize));
        while (textpaint.measureText(longestline) > wdm.getWidgetWidth()*0.9){
            widget_textsize = widget_textsize - 1;
            textpaint.setTextSize(wdm.getFontHeightInPixels(widget_textsize));
        }
        longestline = "";
        if (textpaint.measureText(line2text) > textpaint.measureText(longestline)){
            longestline = line2text;
        }
        if (textpaint.measureText(line3text) > textpaint.measureText(longestline)){
            longestline = line3text;
        }
        if (textpaint.measureText(line4text) > textpaint.measureText(longestline)){
            longestline = line4text;
        }
        if (textpaint.measureText(line5text) > textpaint.measureText(longestline)){
            longestline = line5text;
        }
       while (textpaint.measureText(longestline) > wdm.getWidgetWidth()*0.45){
            widget_textsize = widget_textsize - 1;
           textpaint.setTextSize(wdm.getFontHeightInPixels(widget_textsize));
        }
        // display line #1
        if (line1 != 0){
            rv.setTextViewText(line1,line1text);
            rv.setTextViewTextSize(line1,TypedValue.COMPLEX_UNIT_SP,widget_textsize);
            rv.setTextColor(line1,widget_textcolor);
        }
        // display line #2, left column
        if (line2 != 0){
            rv.setTextViewText(line2,line2text);
            rv.setTextViewTextSize(line2,TypedValue.COMPLEX_UNIT_SP,widget_textsize);
            rv.setTextColor(line2,widget_textcolor);
        }
        // display line #3, left column
        if (line3 != 0){
            rv.setTextViewText(line3,line3text);
            rv.setTextViewTextSize(line3,TypedValue.COMPLEX_UNIT_SP,widget_textsize);
            rv.setTextColor(line3,widget_textcolor);
        }
        // display line #2, right column
        if (maxvalueline1 != 0) {
            rv.setTextViewText(maxvalueline1,line4text);
            rv.setTextViewTextSize(maxvalueline1,TypedValue.COMPLEX_UNIT_SP,widget_textsize);
            rv.setTextColor(maxvalueline1,widget_textcolor);
        }
        // display line #3, right column
        if (maxvalueline2 != 0) {
            rv.setTextViewText(maxvalueline2,line5text);
            rv.setTextViewTextSize(maxvalueline2,TypedValue.COMPLEX_UNIT_SP,widget_textsize);
            rv.setTextColor(maxvalueline2,widget_textcolor);
        }

        // display graph
        if (graph != 0){
            float chart_width =  (float) wdm.getWidgetWidth();
            float chart_height = (float) (wdm.getWidgetHeight()*1.45 - wdm.getFontHeightInPixels(widget_textsize)*3);
            /* The widget should display the chart with (aprrox.) symmetric x- and y- axis.
             * Therefore, height and width should be the same. We apply the larger value, the widget will
             * shrink the bitmap, if necessary.
             */
            if (chart_height>chart_width){
                chart_width = chart_height;
            } else {
                chart_height = chart_width;
            }
            rv.setImageViewBitmap(graph,getCloseSensorsMap(location_data,sensor_data,chart_width,chart_height,zoom));
        }
        if (display_reference){
            rv.setViewVisibility(R.id.widget_close_reference_text, View.VISIBLE);
            rv.setTextColor(R.id.widget_close_reference_text,widget_textcolor);
        } else {
            rv.setViewVisibility(R.id.widget_close_reference_text, View.GONE);
        }
        return rv;
    }
}
