FeinstaubWidget
===============

Displays fine particulate matter values from the https://luftdaten.info project.

Screenshots
-----------

![Screenshot #1](fastlane/metadata/android/en-US/images/phoneScreenshots/1.png)
![Screenshot #2](fastlane/metadata/android/en-US/images/phoneScreenshots/2.png)
![Screenshot #3](fastlane/metadata/android/en-US/images/phoneScreenshots/4.png)

License
-------

FeinstaubWidget

Copyright (c) 2018, 2019 Pawel Dube

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

FeinstaubWidget is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

Credits
-------

The Material Design Icons are (c) Google Inc., licensed under
the Apache License Version 2.0.

This app uses gradle and the gradle wrapper, Copyright (c) Gradle Inc.,
licensed under the Apache 2.0 license.

Collects and displays data from the Luftdaten.Info network
(https://luftdaten.info), a project created by OK Lab Stuttgart
(https://codefor.de/stuttgart/).

For the license for data, see https://luftdaten.info; Luftdaten.Info
makes the data available under the Database Contents License (DbCL)
v1.0 (https://opendatacommons.org/licenses/dbcl/1.0/)).

Getting the Binary
------------------

Imagepipe is available on the F-Droid main repository.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" 
    alt="Get it on F-Droid"
    height="80">](https://f-droid.org/packages/de.kaffeemitkoffein.feinstaubwidget)

From the author's custom F-Droid repository.
--------------------------------------------
Alternatively, the app is also available in the author's custom repository. 

Repo: https://kaffeemitkoffein.de

Fingerprint: 2F 27 5C B8 37 35 FE 79 75 44 9C E8 00 CA 84 07 B5 01 9D 5F 7B 6A BC B3 0F 36 51 C5 20 A3 72 61

### Here is a short instruction how to add this repo:

1. Launch the f-droid app
2. Select "Settings"
3. Select "Repositories" from the list
4. Select "+NEW REPOSITORY"
5. Add the following repository address: "https://kaffeemitkoffein.de"
6. Optional: enter the fingerprint of this repo (see above)
7. Select "Add"

### Now, you can install this app via f-droid as soon as the app updated the repo data:

1. In the main view, update the f-droid repos by swiping down
2. Look for the Feinstaubwidget app in f-droid and install it

Direct download of the binary (APK)
-----------------------------------

You can also directly download the apk file via this link:

<https://kaffeemitkoffein.de/fdroid/repo/Feinstaubwidget.apk>

CONTRIBUTING
============

The source code of Imagepipe is available on codeberg.org:
 
<https://codeberg.org/Starfish/Imagepipe>

The source code can also be downloaded here:
<https://kaffeemitkoffein.de/source/FeinstaubWidget_source.tar.gz>

For suggestions and bug reports, please contact the author:
pawel (at) kaffeemitkoffein.de