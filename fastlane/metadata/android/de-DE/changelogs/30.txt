Neu in Version 0.74:

- minimale Widget-Größe angepasst für bessere Integration in benutzerdefinierte Raster-Größen auf dem Home-Screen
- automatische Update-Versuche im Widget mit nahen Sensoren entfernt, wenn kein Netzwerk verfügbar ist, um die Batterie und die Ressourcen zu schonen
